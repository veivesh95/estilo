const chai = require('chai');
const expect = chai.expect;
const axios = require('axios');
const supertest = require('supertest');
const jsonfile = require('jsonfile');
const chaiShould = chai.should();
const should = require('should');

let loginJson = require('./login');
let newUser = require('./newUser');

describe('Register to estilo', () => {
	// describe('Registering', () => {
	// 	it('returns 200 OK status & valid user JWT token', done => {
	// 		// this.timeout(20000);
	// 		axios
	// 			.post('http://localhost:8000/api/users/signup', newUser)
	// 			.then(res => {
	// 				expect(res.status).to.be.eql(200);
	// 				console.log(res.data);
	// 				done();

	// 				token = {
	// 					headers: { 'x-access-token': res.data.token }
	// 				};

	// 				const file = './test/user/newUserToken.json';
	// 				jsonfile.writeFile(file, token, function(err) {
	// 					if (err) {
	// 						console.error(err);
	// 					}
	// 				});
	// 			})
	// 			.catch(err => {
	// 				console.log(err);
	// 				done();
	// 			});
	// 	});
	// });

	describe('Logging In', () => {
		it('returns 200', done => {
			axios
				.post('http://localhost:8000/api/login', loginJson)
				.then(res => {
					expect(res.status).to.be.eql(200);
					console.log(res.data);
					done();

					token = {
						headers: { 'x-access-token': res.data.token }
					};

					var file = './test/user/loginToken.json';
					jsonfile.writeFile(file, token, function(err) {
						if (err) {
							console.error(err);
						}
					});
				})
				.catch(err => {
					console.error(err);
					done();
				});
		});
	});
});
