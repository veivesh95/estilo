const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// const env = require('dotenv').load();
const cookieParser = require('cookie-parser');
const exjwt = require('express-jwt');
const routerIndex = require('./app/routes/route_index');
const secret = require('./app/config/secret');
const cors = require('cors');

// app.use((req, res, next) => {
// 	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
// 	res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
// 	res.setHeader(
// 		'Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS'
// 	);
// 	next();
// });

app.use(bodyParser.json());

const corsOptions = {
	// origin: 'http://localhost:3000'
	//   optionsSuccessStatus: 200,
	origin: '*',
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
	preflightContinue: false,
	optionsSuccessStatus: 200
};

// app.use((req, res, next) => {
// 	res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
// 	next();
// });

// app.use(
// 	exjwt({
// 		secret: 'hello world !',
// 		credentialsRequired: true
// 	})
// );

// const jwtMW = exjwt({
// 	secret: secret.secret
// });

app.use(cors(corsOptions));
app.use('/', routerIndex);

const db = require('./app/config/db.config.js');

// force: true will drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
db.sequelize.sync().then(() => {
	try {
		console.log('Drop and Resync with { force: true }');
		// createRoles();
		// // createCustomers();
		// createSlots();
		// createLevels();
		// createUsers();
	} catch (error) {
		console.error(error);
	} finally {
		console.log(
			`=======================================================
    ======================== DB READY =====================
    ======================================================`
		);
	}
});

require('./app/routes/customer.route.js')(app);
require('./app/routes/slot.route')(app);
require('./app/routes/salon.route')(app);
require('./app/routes/user.route')(app);
require('./app/routes/role.route')(app);
require('./app/routes/level.route')(app);
require('./app/routes/freelancer.route')(app);
require('./app/routes/freelancerDetail.route')(app);
require('./app/routes/booking.route')(app);

function createCustomers() {
	let customers = [
		{
			id: 1,
			firstname: 'Joe',
			lastname: 'Thomas',
			age: 36
		},
		{
			id: 2,
			firstname: 'Peter',
			lastname: 'Smith',
			age: 18
		},
		{
			id: 3,
			firstname: 'Lauren',
			lastname: 'Taylor',
			age: 31
		},
		{
			id: 4,
			firstname: 'Mary',
			lastname: 'Taylor',
			age: 24
		},
		{
			id: 5,
			firstname: 'David',
			lastname: 'Moore',
			age: 25
		},
		{
			id: 6,
			firstname: 'Holly',
			lastname: 'Davies',
			age: 27
		},
		{
			id: 7,
			firstname: 'Michael',
			lastname: 'Brown',
			age: 45
		}
	];

	// Init data -> save to MySQL
	const Customer = db.customers;
	for (let i = 0; i < customers.length; i++) {
		Customer.create(customers[i]);
	}
}

function createUsers() {
	let users = [
		{
			id: 1,
			roleId: 1,
			username: 'johndoe@gmail.com',
			password: 'abcd1234',
			userEin: '123412341234'
		},
		{
			id: 2,
			roleId: 2,
			username: 'janedoe@gmail.com',
			password: 'abcd1234',
			userEin: '1234123412123'
		},
		{
			id: 3,
			roleId: 2,
			username: 'veivejsads@aasdfasd.com',
			password: '$2b$10$OdDmWgWoT.cMW5DhFWhMq.hgHvUoP6dw56wXlOvT32rbDoiORwvum',
			userEin: '1214'
		},
		{
			id: 4,
			roleId: 1,
			username: 'abcd@abcd.com',
			password: '$2b$10$EvtpOJ0cMghY/lBj8btty.sWGqwRSwWxkK0aNgLfT98W2p3Ub2BFe',
			userEin: '123414'
		},
		{
			id: 5,
			roleId: 2,
			username: 'asdf@asdf.com',
			password: '$2b$10$UbinFkYbIblb.fnhjNkCje2A0w0i9JPTsKy0uVTalT16ZpBlpkILq',
			userEin: 'ein12341234'
		}
	];

	let User = db.users;
	for (let i = 0; i < users.length; i++) {
		User.create(users[i]);
	}
}

function createRoles() {
	let roles = [
		{
			roleId: 1,
			role: 'salon'
		},
		{
			roleId: 2,
			role: 'freelancer'
		}
	];

	let Role = db.roles;
	for (let i = 0; i < roles.length; i++) {
		Role.create(roles[i]);
	}
}

function createLevels() {
	let levels = [
		{
			levelId: 1,
			title: 'stylist'
		},
		{
			levelId: 2,
			title: 'educator'
		}
	];

	let Level = db.levels;
	for (let i = 0; i < levels.length; i++) {
		Level.create(levels[i]);
	}
}

function createSlots() {
	let slots = [
		{
			slotId: 1,
			slot: 'day'
		},
		{
			slotId: 2,
			slot: 'evening'
		}
	];

	let Slot = db.slots;
	for (let i = 0; i < slots.length; i++) {
		Slot.create(slots[i]);
	}
}

var server = app.listen(8000, function() {
	let host = server.address().address;
	let port = server.address().port;

	console.log('App listening at http://%s:%s', host, port);
});
