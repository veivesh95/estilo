const env = require('./env');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
  port: env.port,
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//Models/tables
db.roles = require('../models/role.model')(sequelize, Sequelize);
db.customers = require('../models/customer.model.js')(sequelize, Sequelize);
db.slots = require('../models/slot.model')(sequelize, Sequelize);
db.salons = require('../models/salon.model')(sequelize, Sequelize);
db.users = require('../models/user.model')(sequelize, Sequelize);
db.levels = require('../models/level.model')(sequelize, Sequelize);
// db.address = require('../models/address.model.js')(sequelize, Sequelize);
db.freelancers = require('../models/freelancer.model.js')(sequelize, Sequelize);
db.freelancerDetails = require('../models/freelancerDetail.model')(
  sequelize,
  Sequelize
);
db.bookings = require('../models/booking.model')(sequelize, Sequelize);

module.exports = db;
