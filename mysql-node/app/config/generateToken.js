const jwt = require('jsonwebtoken');
const env = require('./env');
const secret = require('./secret');

exports.generateToken = user => {
  return (token = jwt.sign({ username: user.username }, secret.secret, {
    expiresIn: 60 * 60 * 24
  }));
};
