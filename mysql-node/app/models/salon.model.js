module.exports = (sequelize, Sequelize) => {
	const Salon = sequelize.define('salon', {
		salonId: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},
		userId: {
			type: Sequelize.INTEGER,
			references: {
				constraint: true,
				model: 'users',
				key: 'userId'
			},
			allowNull: false
		},
		salonName: {
			type: Sequelize.STRING,
			allowNull: true
			// validate: { isUppercase: true }
		},
		salonEin: {
			type: Sequelize.STRING,
			unique: true
		},
		salonStreetNumber: {
			type: Sequelize.STRING,
			allowNull: true
		},
		salonStreet: {
			type: Sequelize.STRING,
			allowNull: true
		},
		salonCity: {
			type: Sequelize.STRING,
			allowNull: true
		},
		salonStateCode: {
			type: Sequelize.STRING,
			allowNull: true
		},
		salonZip: {
			type: Sequelize.STRING,
			allowNull: true
		},
		salonContact: {
			type: Sequelize.STRING,
			allowNull: true
		}
	});

	return Salon;
};
