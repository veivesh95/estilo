module.exports = (sequelize, Sequelize) => {
	const Booking = sequelize.define('booking', {
		bookingId: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},
		salonId: {
			type: Sequelize.INTEGER,
			references: {
				constraint: true,
				model: 'salons',
				key: 'salonId'
			},
			allowNull: false
		},
		freelancerId: {
			type: Sequelize.INTEGER,
			references: {
				constraint: true,
				model: 'freelancers',
				key: 'freelancerId'
			},
			allowNull: false
		},
		fdId: {
			type: Sequelize.INTEGER,
			references: {
				constraint: true,
				model: 'freelancerdetails',
				key: 'fdId'
			},
			allowNull: false
		},
		createdDate: {
			type: Sequelize.DATEONLY,
			allowNull: false,
			validate: { isDate: true }
		},
		bookingDate: {
			type: Sequelize.DATEONLY,
			allowNull: false,
			validate: { isDate: true }
		},
		status: {
			type: Sequelize.ENUM('pending', 'accepted', 'cancelled', 'completed'),
			allowNull: false
		},
		paymentStatus: {
			type: Sequelize.ENUM('pending', 'transferred', 'cancelled'),
			allowNull: false
		},
		rating: {
			type: Sequelize.STRING,
			allowNull: false
		},
		amount: {
			type: Sequelize.FLOAT,
			allowNull: false,
			validate: { isFloat: true }
		}
	});

	return Booking;
};
