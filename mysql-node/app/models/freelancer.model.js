module.exports = (sequelize, Sequelize) => {
	const Freelancer = sequelize.define('freelancer', {
		freelancerId: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},
		userId: {
			type: Sequelize.INTEGER,
			references: {
				constraint: true,
				model: 'users',
				key: 'userId'
			},
			allowNull: false
		},
		levelId: {
			type: Sequelize.INTEGER,
			references: {
				constraint: true,
				model: 'levels',
				key: 'levelId'
			},
			allowNull: true
		},
		experience: {
			type: Sequelize.INTEGER,
			allowNull: true
		},
		rating: {
			type: Sequelize.INTEGER,
			allowNull: true
		}
	});

	return Freelancer;
};
