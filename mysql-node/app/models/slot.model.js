module.exports = (sequelize, Sequelize) => {
  const Slot = sequelize.define('slot', {
    slotId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    slot: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });

  return Slot;
};
