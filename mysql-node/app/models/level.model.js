module.exports = (sequelize, Sequelize) => {
  const Level = sequelize.define(
    'level',
    {
      levelId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      }
    }
    // {classMethods:{
    //   associate: function(models){}
    // }}
  );

  return Level;
};
