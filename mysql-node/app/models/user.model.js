module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define(
		'user',
		{
			userId: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			roleId: {
				type: Sequelize.INTEGER,
				references: {
					constraint: true,
					model: 'roles',
					key: 'roleId'
				},
				allowNull: true
			},
			userEin: {
				type: Sequelize.STRING,
				unique: true,
				allowNull: true
			},
			username: {
				type: Sequelize.STRING,
				unique: true,
				validate: { isEmail: true }
			},
			password: {
				type: Sequelize.STRING,
				allowNull: true
			},
			firstName: {
				type: Sequelize.STRING
				// validate: { isUppercase: true }
			},
			lastName: {
				type: Sequelize.STRING
				// validate: { isUppercase: true }
			},
			streetNumber: {
				type: Sequelize.STRING,
				allowNull: true
			},
			street: {
				type: Sequelize.STRING,
				allowNull: true
			},
			city: {
				type: Sequelize.STRING,
				allowNull: true
			},
			stateCode: {
				type: Sequelize.STRING,
				allowNull: true
			},
			zip: {
				type: Sequelize.STRING,
				allowNull: true
			},
			contact: {
				type: Sequelize.STRING,
				allowNull: true
			},
			contactWork: {
				type: Sequelize.STRING,
				allowNull: true
			},
			email: {
				type: Sequelize.STRING,
				allowNull: true
			}
		}
		// {classMethods:{
		//   associate: function(models){}
		// }}
	);

	return User;
};
