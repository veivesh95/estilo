module.exports = (sequelize, Sequelize) => {
  const Role = sequelize.define('role', {
    roleId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });

  return Role;
};
