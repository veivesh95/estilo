module.exports = (sequelize, Sequelize) => {
  const FreelancerDetail = sequelize.define('freelancerDetail', {
    fdId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    freelancerId: {
      type: Sequelize.INTEGER,
      references: {
        constraint: true,
        model: 'freelancers',
        key: 'freelancerId'
      },
      allowNull: false
    },
    slotId: {
      type: Sequelize.INTEGER,
      references: {
        constraint: true,
        model: 'slots',
        key: 'slotId'
      },
      allowNull: false
    },
    levelId: {
      type: Sequelize.INTEGER,
      references: {
        constraint: true,
        model: 'levels',
        key: 'levelId'
      },
      allowNull: false
    },
    charge: {
      type: Sequelize.FLOAT,
      allowNull: true
    }
  });

  return FreelancerDetail;
};
