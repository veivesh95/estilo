const db = require('../config/db.config.js');
const Freelancer = db.freelancers;
const FD = db.freelancerDetails;
const User = db.users;
const sequelize = db.sequelize;
const Sequelize = db.Sequelize;

exports.findAll = (req, res) => {
	Freelancer.findAll().then(freelancers => {
		res.json(freelancers);
	});
};

exports.findById = (req, res) => {
	Freelancer.findById(req.params.freelancerId).then(freelancer => {
		res.json(freelancer);
	});
};

exports.bookingDetails = (req, res) => {
	sequelize
		.query(
			'SELECT freelancerdetails.* FROM freelancerdetails WHERE freelancerdetails.freelancerId = :freelancerId',
			{
				replacements: { freelancerId: req.params.freelancerId },
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(result => {
			res.json(result);
		})
		.catch(err => {
			res.json(err);
		});
};

exports.searchOne = (req, res) => {
	sequelize
		.query(
			'SELECT `freelancers`.`freelancerId`,`slots`.`slot`,`levels`.`title`,`users`.`*`,`freelancers`.`experience`,`freelancers`.`rating`,`freelancerdetails`.`charge`FROM((((`freelancers`INNER JOIN `freelancerdetails` ON `freelancerdetails`.`freelancerId` = `freelancers`.`freelancerId`)INNER JOIN `users` ON `freelancers`.`userId` = `users`.`userId`)INNER JOIN `levels` ON `freelancerdetails`.`levelId` = `levels`.`levelId`)INNER JOIN `slots` ON `freelancerdetails`.`slotId` = `slots`.`slotId`) WHERE `freelancers`.`freelancerId`= :freelancerId ORDER BY `freelancers`.`freelancerId`',
			{
				replacements: { freelancerId: req.params.freelancerId },
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(result => {
			result['password'] = 'HIDDEN';
			res.json(result);
		})
		.catch(err => {
			res.json(err);
		});
};

exports.searchByLevel = (req, res) => {
	let roleId = req.params.id;
	sequelize
		.query(
			'SELECT freelancers.freelancerId,  freelancers.userId, freelancers.levelId,  freelancers.experience, freelancers.rating, users.userId, users.userEin, users.username,  users.password, users.firstName, users.lastName, users.streetNumber, users.street, users.city, users.stateCode, users.zip, users.contact, users.contactWork, users.email FROM freelancers, users WHERE freelancers.userId = users.userId AND freelancers.levelId = :levelId',
			{
				replacements: { levelId: roleId },
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(done => {
			res.json({ freelancers: done });
		})
		.catch(err => {
			res.json(err);
		});
};

exports.searchByDate = (req, res) => {
	// let date = req.params.date;
	let date = req.body.date;

	let query =
		'SELECT freelancers.freelancerId, freelancers.userId, freelancers.levelId, freelancers.experience, freelancers.rating, users.userId, users.userEin, users.username, users.password, users.firstName, users.lastName, users.streetNumber, users.street, users.city, users.stateCode, users.zip, users.contact, users.contactWork, users.email FROM freelancers, users WHERE freelancers.userId = users.userId AND freelancers.freelancerId NOT IN (SELECT freelancers.freelancerId FROM (freelancers INNER JOIN bookings ON bookings.freelancerId = freelancers.freelancerId) WHERE bookings.bookingDate = ' +
		date +
		')';

	sequelize
		.query(query)
		.then(done => {
			res.json({ freelancers: done });
		})
		.catch(err => {
			res.json(err);
		});
};

exports.searchByLevelAndDate = (req, res) => {
	let levelId = req.body.levelId;
	let date = req.body.date;

	let query =
		'SELECT freelancers.freelancerId, freelancers.userId, freelancers.levelId, freelancers.experience, freelancers.rating, users.userId, users.userEin, users.username, users.password, users.firstName, users.lastName, users.streetNumber, users.street, users.city, users.stateCode, users.zip, users.contact, users.contactWork, users.email FROM freelancers, users WHERE freelancers.userId = users.userId AND freelancers.levelId =' +
		levelId +
		' AND freelancers.freelancerId NOT IN (SELECT freelancers.freelancerId FROM (freelancers INNER JOIN bookings ON bookings.freelancerId = freelancers.freelancerId)  WHERE bookings.bookingDate = ' +
		date +
		')';

	sequelize
		.query(query)
		.then(done => {
			res.json({ freelancers: done });
		})
		.catch(err => {
			res.json(err);
		});
};

exports.searchByLevelAndRating = (req, res) => {
	let levelId = req.body.levelId;
	let rating = req.body.rating;

	sequelize
		.query(
			'SELECT freelancers.freelancerId,  freelancers.userId, freelancers.levelId,  freelancers.experience, freelancers.rating, users.userId, users.userEin, users.username,  users.password, users.firstName, users.lastName, users.streetNumber, users.street, users.city, users.stateCode, users.zip, users.contact, users.contactWork, users.email FROM freelancers, users WHERE freelancers.userId = users.userId AND freelancers.levelId = :levelId AND freelancers.rating >= :rating',
			{
				replacements: { levelId: levelId, rating: rating },
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(done => {
			res.json({ freelancers: done });
		})
		.catch(err => {
			res.json(err);
		});
};

exports.search = (req, res) => {
	// var query = "SELECT "
	sequelize
		.query(
			'SELECT `freelancers`.`freelancerId`,`slots`.`slot`,`levels`.`title`,`users`.`*`,`freelancers`.`experience`,`freelancers`.`rating`,`freelancerdetails`.`charge`FROM((((`freelancers`INNER JOIN `freelancerdetails` ON `freelancerdetails`.`freelancerId` = `freelancers`.`freelancerId`)INNER JOIN `users` ON `freelancers`.`userId` = `users`.`userId`)INNER JOIN `levels` ON `freelancerdetails`.`levelId` = `levels`.`levelId`)INNER JOIN `slots` ON `freelancerdetails`.`slotId` = `slots`.`slotId`)ORDER BY `freelancers`.`freelancerId`',
			{ type: sequelize.QueryTypes.SELECT }
		)
		.then(result => {
			result['password'] = 'HIDDEN';
			res.json(result);
		})
		.catch(err => {
			res.json(err);
		});

	// let roleId = req.params.id;

	// Freelancer.findOne({
	// 	include: {
	// 		required: true,
	// 		model: User,
	// 		as: 'users',
	// 		where: {
	// 			userId: req.params.id
	// 		}

	// 		// include: {
	// 		// 	required: true,
	// 		// 	model: models.states,
	// 		// 	as: 'states',
	// 		//
	// 		// }
	// 	}
	// })
	// 	.then(result => {
	// 		res.json(result);
	// 	})
	// 	.catch(function(error) {
	// 		console.log('error getTotalPlayer', error);
	// 	});

	// Freelancer.findAll({
	// 	include: [
	// 		{
	// 			model: User,
	// 			on: {
	// 				col1: sequelize.where(
	// 					sequelize.col('freelancers.freelancerId'),
	// 					'=',
	// 					sequelize.col('users.freelancerId')
	// 				)
	// 			},
	// 			attributes: ['firstName', 'lastName'] // empty array means that no column from ModelB will be returned
	// 		}
	// 	]
	// })
	// 	.then(result => {
	// 		res.json(result);
	// 	})
	// 	.catch(err => {
	// 		res.json(err);
	// 	});

	// Freelancer.findAll({
	// 	include: [
	// 		{
	// 			model: User,
	// 			where: {
	// 				roleId: 2
	// 			}
	// 		}
	// 	]
	// }).then(result => {
	// 	res.json(result);
	// });
};

// Find all projects with a least one task where task.state === project.state
// Project.findAll({
// 	include: [
// 		{
// 			model: Task,
// 			where: { state: Sequelize.col('project.state') }
// 		}
// 	]
// });

exports.getfreelancerInfo = (req, res) => {
	Freelancer.findOne({ where: { userId: req.params.id } }).then(freelancer => {
		let freelancerId = freelancer.freelancerId;

		sequelize
			.query(
				'SELECT freelancers.*, users.* FROM freelancers, users where users.userId = freelancers.userId and freelancers.freelancerId= :freelancerId',
				{
					replacements: {
						freelancerId: freelancerId
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(done => {
				res.json(done[0]);
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.getfreelancerInfo2 = (req, res) => {
	let freelancerId = req.params.id;

	sequelize
		.query(
			'SELECT freelancers.*, users.* FROM freelancers, users where users.userId = freelancers.userId and freelancers.freelancerId= :freelancerId',
			{
				replacements: {
					freelancerId: freelancerId
				},
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(done => {
			res.json(done[0]);
		})
		.catch(err => {
			res.json(err);
		});
};

exports.searchNew = (req, res) => {
	let query =
		'SELECT freelancers.*, users.* FROM freelancers, users WHERE freelancers.userId = users.userId';
	sequelize
		.query(query, { type: sequelize.QueryTypes.SELECT })
		.then(freelancers => {
			res.json({ freelancers: freelancers });
		})
		.catch(err => {
			res.json(err);
		});
};

exports.update = (req, res) => {
	let currentUserId = req.params;
	// console.log(currentUserId);

	const stylistDayPay = req.body.stylistDayPay;
	const stylistEvePay = req.body.stylistEvePay;
	const educatorDayPay = req.body.educatorDayPay;
	const educatorEvePay = req.body.educatorDayPay;

	const checker = req.body.levelId === 1 ? true : false;

	Freelancer.findOne({ where: { userId: req.params.userId } })
		.then(freelancer => {
			freelancer
				.updateAttributes({
					levelId: req.body.levelId,
					experience: req.body.xp
				})
				.then(change => {
					let freelancerId = change.freelancerId;
					let levelId = change.levelId;

					res.json(change);

					if (checker) {
						FD.create({
							freelancerId: freelancerId,
							slotId: 1,
							levelId: 1,
							charge: stylistDayPay
						}).then(
							FD.create({
								freelancerId: freelancerId,
								slotId: 2,
								levelId: 1,
								charge: stylistEvePay
							})
						);
					} else {
						FD.create({
							freelancerId: freelancerId,
							slotId: 1,
							levelId: 1,
							charge: stylistDayPay
						}).then(
							FD.create({
								freelancerId: freelancerId,
								slotId: 2,
								levelId: 1,
								charge: stylistEvePay
							}).then(
								FD.create({
									freelancerId: freelancerId,
									slotId: 1,
									levelId: 2,
									charge: educatorDayPay
								}).then(
									FD.create({
										freelancerId: freelancerId,
										slotId: 2,
										levelId: 2,
										charge: educatorEvePay
									}).catch(err => {
										res.json(err);
									})
								)
							)
						);
					}
				})
				.catch(err => {
					res.json(err);
				});
		})
		.catch(err => {
			res.json(err);
		});

	// Freelancer.findById(req.params.freelancerId)
	// 	.then(freelancer => {
	// 		freelancer
	// 			.updateAttributes({
	// 				levelId: req.body.levelId,
	// 				experience: req.body.xp
	// 			})
	// 			.then(change => {
	// 				let freelancerId = change.freelancerId;
	// 				let levelId = change.levelId;

	// 				res.json(change);
	// 				// FD.create();
	// 			})
	// 			.catch(err => {
	// 				res.json(err);
	// 			});
	// 	})
	// 	.catch(err => {
	// 		res.json(err);
	// 	});
};
