const db = require('../config/db.config.js');
const Role = db.roles;

exports.findAll = (req, res) => {
  Role.findAll().then(roles => {
    res.json(roles);
  });
};

exports.findById = (req, res) => {
  Role.findById(req.params.roleId).then(role => {
    res.json(role);
  });
};
