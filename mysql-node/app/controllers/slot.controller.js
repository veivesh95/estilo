const db = require('../config/db.config.js');
const Slot = db.slots;

exports.findAll = (req, res) => {
	Slot.findAll().then(slots => {
		res.json(slots);
	});
};

exports.findById = (req, res) => {
	Slot.findById(req.params.slotId).then(slot => {
		res.json(slot);
	});
};
