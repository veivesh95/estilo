const db = require('../config/db.config.js');
const Customer = db.customers;
const Address = db.address;
// Post a Customer
exports.create = (req, res) => {
	// Save to MySQL database
	let customer;
	Customer.create({
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		age: req.body.age
	})
		.then(result => {
			// Send created customer to client
			customer = result;
			return Address.create({
				street: req.body.street,
				phone: req.body.phone
			});
			// res.json(result);
		})
		.then(address => {
			customer.setAddress(address);
			res.send('OK');
		});
};

// FETCH all Customers include Addresses
exports.findAll = (req, res) => {
	Customer.findAll({
		attributes: [['id', 'customerId'], ['firstname', 'name'], 'age'],
		include: [
			{
				model: Address,
				where: { fk_customerid: db.Sequelize.col('customer.uuid') },
				attributes: ['street', 'phone']
			}
		]
	})
		.then(customers => {
			res.send(customers);
		})
		.catch(err => {
			res.json(err.original.sqlMessage);
		});
};
// // Fetch all Customers
// exports.findAll = (req, res) => {
// 	Customer.findAll().then(customers => {
// 		// Send all customers to Client
// 		res.json(customers);
// 	});
// };

// Find a Customer by Id
exports.findById = (req, res) => {
	Customer.findById(req.params.customerId).then(customer => {
		res.json(customer);
	});
};

// Update a Customer
exports.update = (req, res) => {
	let customer = req.body;
	let id = req.body.id;
	Customer.update(customer, { where: { id: id } }).then(() => {
		res
			.status(200)
			.json({ msg: 'updated successfully a customer with id = ' + id });
	});
};

// Delete a Customer by Id
exports.delete = (req, res) => {
	const id = req.params.customerId;
	Customer.destroy({
		where: { id: id }
	}).then(() => {
		res
			.status(200)
			.json({ msg: 'deleted successfully a customer with id = ' + id });
	});
};
