const db = require('../config/db.config.js');
const User = db.users;
const bcrypt = require('bcrypt');
const config = require('../config/generateToken');
const jwt = require('jsonwebtoken');
const secret = require('../config/secret');
const VerifyToken = require('../config/verifyToken');
const Freelancer = db.freelancers;
const Salon = db.salons;

exports.findAll = (req, res) => {
	User.findAll().then(users => {
		res.json(users);
	});
};

exports.findById = (req, res) => {
	User.findById(req.params.userId).then(user => {
		user['password'] = 'HIDDEN';
		res.json(user);
	});
};

exports.findByUsername = (req, res) => {
	User.findOne({ where: { username: req.params.username } })
		.then(user => {
			let userObj = user;
			res.status(200).send({ userObj });
		})
		.catch(err => {
			res.status(500).send(err);
		});
};

exports.create = (req, res, next) => {
	let user = req.body;

	if (user.password === null || user.password === '') {
		return res.json('Please specify a valid password');
	} else {
		let hash = bcrypt.hashSync(user.password, 10);
		user['password'] = hash;

		// res.json(user);
		User.create(user)
			.then(created => {
				let token = config.generateToken(created);

				let roleId = created.roleId;
				let createdId = { userId: created.userId };

				if (roleId === 1) {
					Salon.create(createdId)
						.then(done => {
							return res.json({ created, done, token: token });
						})
						.catch(err => {
							return res.json(err);
						});
				} else if (roleId === 2) {
					Freelancer.create(createdId)
						.then(done => {
							return res.json({ created, done, token: token });
						})
						.catch(err => {
							return res.json(err.name);
						});
				}

				return res.json({ created, token: token });
			})
			.catch(err => {
				res.json(err.name);
			});
	}
};

exports.login = (req, res) => {
	// const user = req.body;
	User.findOne({ where: { username: req.body.username } })
		.then(user => {
			let password = user.password;
			let passwordIsValid = bcrypt.compareSync(req.body.password, password);

			if (!passwordIsValid) {
				return res.status(401).send({ auth: false, token: null });
			}
			var token = jwt.sign({ username: user.username }, secret.secret, {
				expiresIn: 60 * 60 * 24
			});
			res.status(200).send({ auth: true, user: user, token: token });
		})
		.catch(err => {
			res.status(500).send(err);
		});
};

exports.logout = (req, res) => {
	res.status(200).send({ auth: false, token: null });
};

exports.whoami = (req, res, next) => {
	// res.json('asd');
	User.findOne({ where: { username: req.username } })
		.then(user => {
			user['password'] = 'HIDDEN';
			res.json(user);
		})
		.catch(err => {
			res.json(err);
		});
};

exports.update = (req, res) => {
	User.findById(req.params.userId)
		.then(user => {
			user
				.updateAttributes({
					roleId: req.body.roleId,
					userEin: req.body.userEin,
					username: req.body.username,
					password: req.body.password,
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					streetNumber: req.body.streetNumber,
					street: req.body.street,
					city: req.body.city,
					stateCode: req.body.stateCode,
					zip: req.body.zip,
					contact: req.body.contact,
					contactWork: req.body.contactWork,
					email: req.body.email
				})
				.then(change => {
					res.json(change);
				})
				.catch(err => {
					res.json(err);
				});
		})
		.catch(err => {
			res.json(err);
		});

	// User.update(
	// 	{ firstName:  },
	// 	{ returning: true, where: { userId: req.params.userId } }
	// )
	// 	.then(function([rowsUpdate, [updatedUser]]) {
	// 		res.json(updatedUser);
	// 	})
	// 	.catch(err => {
	// 		res.json(err);
	// 	});
	// res.json(user);

	// User.find({ where: { userId: req.params.userId } }).on('success', function(
	// 	user
	// ) {});
};

// router.put('/book/:bookId', function(req, res, next) {
// 	Book.update(
// 		{ title: req.body.title },
// 		{ returning: true, where: { id: req.params.bookId } }
// 	)
// 		.then(function([rowsUpdate, [updatedBook]]) {
// 			res.json(updatedBook);
// 		})
// 		.catch(next);
// });

// exports.signIn = (req, res, next) => {
//   let user = req.body;
//   User.findOne({ username: user.username }).exec(function(err, user) {
//     if (err) {
//       res.json({ err: err });
//     }
//     if (!user) {
//       return res.status(404).json({
//         error: true,
//         message: 'Username or Password is Wrong'
//       });
//     }
//     bcrypt.compare(req.body.password, user.password, function(err, valid) {
//       if (err) {
//         res.json({ err: err });
//       }

//       if (!valid) {
//         return res.status(404).json({
//           error: true,
//           message: 'Username or Password is Wrong'
//         });
//       }

//       var token = config.generateToken(user);
//       user = config.getCleanUser(user);
//       res.json({
//         user: user,
//         token: token
//       });
//     });
//   });
// };
