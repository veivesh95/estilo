const db = require('../config/db.config.js');
const Salon = db.salons;
const sequelize = db.sequelize;

exports.findAll = (req, res) => {
	Salon.findAll().then(salons => {
		res.json(salons);
	});
};

exports.findById = (req, res) => {
	Salon.findById(req.params.salonId).then(salon => {
		res.json(salon);
	});
};

exports.create = (req, res) => {
	let salonDetails = req.body;
	Salon.create(salonDetails)
		.then(response => {
			res.json(response);
		})
		.catch(err => {
			res.json(err.original.sqlMessage);
		});
};

exports.getSalonInfo = (req, res) => {
	Salon.findOne({ where: { userId: req.params.id } }).then(salon => {
		let salonId = salon.salonId;

		sequelize
			.query(
				'SELECT salons.*, users.* FROM salons, users where users.userId = salons.userId and salons.salonId= :salonId',
				{
					replacements: {
						salonId: salonId
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(done => {
				res.json(done[0]);
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.update = (req, res) => {
	let currentUserId = req.params.cid;
	let sd = req.body;

	Salon.findOne({ where: { userId: currentUserId } })
		.then(salon => {
			salon
				.updateAttributes({
					salonName: sd.salonName,
					salonEin: sd.salonEIN,
					salonStreetNumber: sd.salonStreetNumber,
					salonStreet: sd.salonStreet,
					salonCity: sd.salonCity,
					salonStateCode: sd.salonStateCode,
					salonZip: sd.salonZip,
					salonContact: sd.salonContact
				})
				.then(changes => {
					res.json(changes);
				})
				.catch(err => {
					res.json(err);
				});
		})
		.catch(err => {
			res.json(err);
		});
};

// exports.create = (req, res) => {
//   // Save to MySQL database
//   let customer = req.body;
//   Customer.create(customer).then(result => {
//     // Send created customer to client
//     res.json(result);
//   });
// };

exports.checkCreate = (req, res) => {
	let salonDetails = req.body;
	Salon.findOrCreate({
		// where: {salonDetails}
	}).then(response => {
		res.json(response);
	});

	// spread((salon, created) => {
	//     console.log(
	//         salon.get({
	//             plain: true
	//         })
	//     );
	//     console.log(created);
	//
	//     /*
	//      findOrCreate returns an array containing the object that was found or created and a boolean that will be true if a new object was created and false if not, like so:
	//
	//     [ {
	//         username: 'sdepold',
	//         job: 'Technical Lead JavaScript',
	//         id: 1,
	//         createdAt: Fri Mar 22 2013 21: 28: 34 GMT + 0100(CET),
	//         updatedAt: Fri Mar 22 2013 21: 28: 34 GMT + 0100(CET)
	//       },
	//       true ]
	//
	//  In the example above, the "spread" on line 39 divides the array into its 2 parts and passes them as arguments to the callback function defined beginning at line 39, which treats them as "user" and "created" in this case. (So "user" will be the object from index 0 of the returned array and "created" will equal "true".)
	//     */
	// });
};
