const db = require('../config/db.config.js');
const FD = db.freelancerDetails;

exports.findAll = (req, res) => {
  FD.findAll().then(fds => {
    res.json(fds);
  });
};

exports.findById = (req, res) => {
  FD.findById(req.params.fsId).then(fd => {
    res.json(fd);
  });
};
