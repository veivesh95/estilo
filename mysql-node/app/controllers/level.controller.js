const db = require('../config/db.config.js');
const Level = db.levels;

exports.findAll = (req, res) => {
	Level.findAll().then(levels => {
		res.json(levels);
	});
};

exports.findById = (req, res) => {
	Level.findById(req.params.levelId).then(level => {
		res.json(level);
	});
};
