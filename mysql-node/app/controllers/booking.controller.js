const db = require('../config/db.config.js');
const Booking = db.bookings;
const FD = db.freelancerDetails;
const Freelancer = db.freelancers;
const Salon = db.salons;
const sequelize = db.sequelize;
const moment = require('moment');

exports.findAll = (req, res) => {
	Booking.findAll().then(bookings => {
		res.json(bookings);
	});
};

exports.findById = (req, res) => {
	Booking.findById(req.params.bookingId).then(booking => {
		res.json(booking);
	});
};

exports.check = (req, res) => {
	sequelize
		.query(
			'SELECT bookings.bookingId,	bookings.salonId, bookings.freelancerId, bookings.fdId,	bookings.bookingDate, bookings.status,	freelancerdetails.slotId, freelancerdetails.levelId FROM (bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) WHERE bookings.freelancerId = :freelancerId and bookings.bookingDate = :bookingDate',
			{
				replacements: {
					freelancerId: req.body.freelancerId,
					bookingDate: req.body.bookingDate
				},
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(result => {
			res.json(result);
		})
		.catch(err => {
			res.json(err);
		});
};

exports.countMyBookingSalon = (req, res) => {
	Salon.findOne({ where: { userId: req.params.id } }).then(salon => {
		let salonId = salon.salonId;

		sequelize
			.query(
				`SELECT  COUNT(bookings.bookingId) as'count' FROM (((bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) INNER JOIN freelancers ON freelancers.freelancerId = bookings.freelancerId)    INNER JOIN users ON freelancers.userId = users.userId)WHERE bookings.salonId = :salonId`,
				{
					replacements: {
						salonId: salonId
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(done => {
				res.json(done[0]);
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.countMyBookingFreelancer = (req, res) => {
	Freelancer.findOne({ where: { userId: req.params.id } }).then(freelancer => {
		let freelancerId = freelancer.freelancerId;

		sequelize
			.query(
				`SELECT  COUNT(bookings.bookingId) as'count' FROM (((bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) INNER JOIN freelancers ON freelancers.freelancerId = bookings.freelancerId)    INNER JOIN users ON freelancers.userId = users.userId)WHERE bookings.freelancerId = :freelancerId`,
				{
					replacements: {
						freelancerId: freelancerId
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(done => {
				res.json(done[0]);
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.countMyBookingFreelancer2 = (req, res) => {
	let freelancerId = req.params.id;

	sequelize
		.query(
			`SELECT  COUNT(bookings.bookingId) as'count' FROM (((bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) INNER JOIN freelancers ON freelancers.freelancerId = bookings.freelancerId)    INNER JOIN users ON freelancers.userId = users.userId)WHERE bookings.freelancerId = :freelancerId`,
			{
				replacements: {
					freelancerId: freelancerId
				},
				type: sequelize.QueryTypes.SELECT
			}
		)
		.then(done => {
			res.json(done[0]);
		})
		.catch(err => {
			res.json(err);
		});
};

exports.myBookings = (req, res) => {
	Salon.findOne({ where: { userId: req.params.id } }).then(salon => {
		let salonId = salon.salonId;

		sequelize
			.query(
				'SELECT  bookings.bookingId, bookings.salonId, bookings.freelancerId, users.firstName, users.lastName, bookings.fdId, bookings.createdDate, bookings.bookingDate, bookings.status, bookings.paymentStatus, bookings.rating, bookings.amount, freelancerdetails.slotId, freelancerdetails.levelId FROM (((bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) INNER JOIN freelancers ON freelancers.freelancerId = bookings.freelancerId) INNER JOIN users ON freelancers.userId = users.userId) WHERE bookings.salonId = :salonId ORDER BY bookings.bookingId desc',
				{
					replacements: {
						salonId: salonId
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(done => {
				res.json({ bookings: done });
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.myBookings2 = (req, res) => {
	Freelancer.findOne({ where: { userId: req.params.id } }).then(freelancer => {
		let freelancerId = freelancer.freelancerId;

		sequelize
			.query(
				'SELECT  bookings.bookingId, bookings.salonId, bookings.freelancerId, users.firstName, users.lastName, bookings.fdId, bookings.createdDate, bookings.bookingDate, bookings.status, bookings.paymentStatus, bookings.rating, bookings.amount, freelancerdetails.slotId, freelancerdetails.levelId FROM (((bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) INNER JOIN freelancers ON freelancers.freelancerId = bookings.freelancerId) INNER JOIN users ON freelancers.userId = users.userId) WHERE bookings.freelancerId = :freelancerId ORDER BY bookings.bookingId desc',
				{
					replacements: {
						freelancerId: freelancerId
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(done => {
				res.json({ bookings: done });
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.currentBookings = (req, res) => {
	console.log(req.body);
	Salon.findOne({ where: { userId: req.body.currentUserId } }).then(salon => {
		let salonId = salon.salonId;

		sequelize
			.query(
				'SELECT bookings.bookingId,bookings.salonId, bookings.freelancerId,bookings.fdId, bookings.createdDate ,bookings.bookingDate,bookings.status,freelancerdetails.slotId,freelancerdetails.levelId FROM(bookings INNER JOIN freelancerdetails ON freelancerdetails.fdId = bookings.fdId) WHERE bookings.salonId = :salonId and bookings.freelancerId=:freelancerId and bookings.createdDate = :createdDate',
				{
					replacements: {
						salonId: salonId,
						freelancerId: req.body.freelancerId,
						createdDate: moment().format('YYYY-MM-DD')
					},
					type: sequelize.QueryTypes.SELECT
				}
			)
			.then(result => {
				res.json({ bookings: result });
			})
			.catch(err => {
				res.json(err);
			});
	});
};

exports.create = (req, res) => {
	const selectedSessionLevel = req.body.selectedSessionLevel;
	const selectedSessionSlot = req.body.selectedSessionSlot;
	const freelancerId = req.body.freelancerId;

	Salon.findOne({ where: { userId: req.body.currentUserId } }).then(salon => {
		let salonId = salon.salonId;

		FD.findOne({
			where: {
				slotId: selectedSessionSlot,
				levelId: selectedSessionLevel,
				freelancerId: freelancerId
			}
		}).then(fd => {
			let fdId = fd.fdId;
			let charge = fd.charge;

			Booking.create({
				salonId: salonId,
				freelancerId: freelancerId,
				fdId: fdId,
				createdDate: moment().format('YYYY-MM-DD'),
				bookingDate: req.body.selectedDate,
				status: 'pending',
				paymentStatus: 'pending',
				rating: 0,
				amount: charge
			})
				.then(rs => {
					res.json(rs);
				})
				.catch(err => {
					res.json(err);
				});
		});
	});
};
