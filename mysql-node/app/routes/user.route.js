module.exports = function(app) {
	const users = require('../controllers/user.controller');

	app.get('/api/users', users.findAll);
	app.get('/api/users/:userId', users.findById);
	app.get('/api/users2/:username', users.findByUsername);
	app.put('/api/user/:userId', users.update);

	app.post('/api/login', users.login);
	app.post('/api/users/signup', users.create);
	// app.get('/api/auth/whoami', users.whoami);
	app.get('/logout', users.logout);
	// app.post('/api/users2', users.create);
};
