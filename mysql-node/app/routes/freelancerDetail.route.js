module.exports = function(app) {
	const fd = require('../controllers/freelancerDetail.controller');

	app.get('/api/fd', fd.findAll);

	app.get('/api/fd/:fdId', fd.findById);
};
