var express = require('express');
var router = express.Router();
let customerRouter = require('../routes/customer.route');
const verifyToken = require('../config/verifyToken');

// router.use("/api", customerRouter);
let customerController = require('../controllers/customer.controller');
router.get('/api/v2/customers', customerController.findAll);

router.get(
  '/api/auth/whoami',
  verifyToken,
  require('../controllers/user.controller').whoami
);

// var task = require('../controllers/task');
// var feedback = require('../controllers/feedback');
// var notice = require('../controllers/notice');
// var taskCategory = require('../controllers/task_category');
// var user = require('../controllers/user');
//
// /*task list*/
// router.get('/task/list', task.list);
// /*task one*/
// router.get('/task/one', task.getOne);
// app.get('/api/auth/whoami', users.whoami);
/*task mod*/
module.exports = router;
