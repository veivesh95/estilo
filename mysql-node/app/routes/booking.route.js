module.exports = function(app) {
	const bookings = require('../controllers/booking.controller');

	app.get('/api/bookings', bookings.findAll);
	app.get('/api/bookings/myBookings/:id', bookings.myBookings);
	app.get('/api/bookings/myBookings2/:id', bookings.myBookings2);

	app.get('/api/bookings/:bookingId', bookings.findById);
	app.post('/api/bookings', bookings.create);
	app.post('/api/bookings/search', bookings.check);
	app.post('/api/bookings/getTodayBooking', bookings.currentBookings);
	app.get('/api/bookings/countSalonBookings/:id', bookings.countMyBookingSalon);
	app.get(
		'/api/bookings/countFreelancerBookings/:id',
		bookings.countMyBookingFreelancer
	);
	app.get(
		'/api/bookings/countFreelancerBookings2/:id',
		bookings.countMyBookingFreelancer2
	);
};
