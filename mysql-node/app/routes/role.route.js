module.exports = function(app) {
  const roles = require('../controllers/role.controller');

  app.get('/api/roles', roles.findAll);

  app.get('/api/roles/:roleId', roles.findById);
};
