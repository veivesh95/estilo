module.exports = function(app) {
	const freelancers = require('../controllers/freelancer.controller');

	app.get('/api/freelancers', freelancers.findAll);
	app.get('/api/freelancers/:freelancerId', freelancers.findById);
	app.get('/api/freelancers123', freelancers.searchNew);
	app.get('/api/freelancers/all', freelancers.search);
	app.put('/api/freelancers/:userId', freelancers.update);
	app.get('/api/freelancers/all/:freelancerId', freelancers.searchOne);
	app.get(
		'/api/freelancers/bookingDetails/:freelancerId',
		freelancers.bookingDetails
	);
	app.post('/api/freelancers/searchByDate', freelancers.searchByDate);
	app.post(
		'/api/freelancers/searchByLevelAndDate',
		freelancers.searchByLevelAndDate
	);
	app.get('/api/freelancers/searchByLevel/:id', freelancers.searchByLevel);
	app.post(
		'/api/freelancers/searchByLevelAndRating',
		freelancers.searchByLevelAndRating
	);
	app.get('/api/freelancerUserInfo/:id', freelancers.getfreelancerInfo);
	app.get('/api/freelancerUserInfo2/:id', freelancers.getfreelancerInfo2);
};
