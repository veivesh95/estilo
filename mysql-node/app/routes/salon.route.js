module.exports = function(app) {
	const salons = require('../controllers/salon.controller');

	app.get('/api/salons', salons.findAll);

	app.get('/api/salons/:salonId', salons.findById);
	app.put('/api/salons/:cid', salons.update);
	app.post('/api/salons', salons.create);
	app.get('/api/salonUserInfo/:id', salons.getSalonInfo);
};
