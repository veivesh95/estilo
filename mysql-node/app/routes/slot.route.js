module.exports = function(app) {
	const slots = require('../controllers/slot.controller');

	app.get('/api/slots', slots.findAll);

	app.get('/api/slots/:slotId', slots.findById);
};
