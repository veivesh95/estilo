module.exports = function(app) {
	const levels = require('../controllers/level.controller');

	app.get('/api/levels', levels.findAll);

	app.get('/api/levels/:levelId', levels.findById);
};
