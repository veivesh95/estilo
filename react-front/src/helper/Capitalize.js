import React from 'react';

const Capitalize = props => {
	let string = props;

	return string
		.charAt(0)
		.toUpperCase()
		.string.slice(1);
};

export default Capitalize;
