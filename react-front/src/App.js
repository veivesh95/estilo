import React, { Fragment, Component } from 'react';
import { BrowserRouter, withRouter } from 'react-router-dom';
import './App.scss';
import Header from './components/elements/Header';
import Footer from './components/elements/Footer';
import MainRouter from './components/pages/MainRouter';
import Auth from './services/Auth';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			hideNav: true
		};

		this.auth = new Auth();
	}

	componentWillMount() {
		if (this.auth.isLoggedIn()) {
			this.setState({ hideNav: false });
		}
	}

	render() {
		const HeaderWithRouter = withRouter(Header);
		let nav = this.state.hideNav ? null : <HeaderWithRouter />;

		return (
			<BrowserRouter>
				<Fragment>
					{nav}
					<MainRouter />
					{/* <Footer /> */}
				</Fragment>
			</BrowserRouter>
		);
	}
}

export default App;
