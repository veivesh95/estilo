import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
	Container,
	Col,
	Form,
	FormGroup,
	FormText,
	FormFeedback,
	Label,
	Input,
	Button
} from 'reactstrap';
import Axios from 'axios';
import Auth from '../../services/Auth';
import { Select, notification } from 'antd';

const Option = Select.Option;

class Signup extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: '',
			confirmPassword: '',
			roleId: '',
			validate: {
				emailState: '',
				passwordState: ''
			},
			disable: true,
			freelancerId: null
		};
		this.handleChange = this.handleChange.bind(this);
		this.validatePassword = this.validatePassword.bind(this);
		this.handleSelectChange = this.handleSelectChange.bind(this);
		this.auth = new Auth();
	}

	validateEmail(e) {
		const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const { validate } = this.state;
		if (emailRex.test(e.target.value)) {
			validate.emailState = 'has-success';
		} else {
			validate.emailState = 'has-danger';
		}
		this.setState({ validate });
	}

	handleChange = async event => {
		const { target } = event;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const { name } = target;
		await this.setState({
			[name]: value
		});
	};

	handleSelectChange(value) {
		this.setState({ disable: false, roleId: value });
	}

	validatePassword(event) {
		const { password, confirmPassword } = this.state;
		console.log(password, ', ', confirmPassword);
		if (password !== confirmPassword) {
			this.setState({
				disable: true,
				validate: { passwordState: 'has-error' }
			});
		} else {
			this.setState({ disable: false });
		}
	}

	submitMethod = e => {
		e.preventDefault();

		Axios.post('http://localhost:8000/api/users/signup', {
			username: this.state.email,
			password: this.state.password,
			roleId: Number(this.state.roleId),
			email: this.state.email
		})
			.then(res => {
				notification.success({
					className: 'notification-global',
					message: 'Hello there! Welcome to estilo',
					onClick: () => {
						console.log('Notification Clicked!');
					}
				});
				console.log(res.data);
				let data = res.data;
				this.auth.setToken(res.data.token);
				// this.setState({ freelancerId: res.data.done.freelancerId });
				// if (data.created.roleId === '1') {
				// 	let state = this.state;
				this.props.history.push('/signup/user-details');
				// }
				// if (data.created.roleId === '2') {
				// 	let state = this.state;
				// 	this.props.history.push({
				// 		pathname: '/salon/details',
				// 		state: this.state
				// 	});
				// }
			})
			.catch(err => {
				notification.error({
					message: 'Signup failed',
					description: err,
					onClick: () => {
						console.log('Notification Clicked!');
					}
				});
				console.log(err);
			});
		// this.props.history.push('/signup/profile');
	};

	render() {
		return (
			<Container>
				<div className='login-container'>
					<Col md={{ offset: 3, size: 6 }}>
						<h2>
							Register in <strong>estilo</strong>
						</h2>
						<Form className='form' onSubmit={this.submitMethod}>
							<FormGroup>
								<Label>Username</Label>
								<Input
									autoFocus
									type='email'
									name='email'
									id='email'
									placeholder='myemail@email.com'
									value={this.state.email}
									// valid={this.state.validate.emailState === 'has-success'}
									invalid={this.state.validate.emailState === 'has-danger'}
									onChange={e => {
										this.validateEmail(e);
										this.handleChange(e);
									}}
								/>
								<FormFeedback valid>
									{/* That's a tasty looking email you've got there. */}
								</FormFeedback>
								<FormFeedback>Give us a valid email address</FormFeedback>
								{/* <FormText>Your username is most likely your email.</FormText> */}
							</FormGroup>

							<FormGroup>
								<Label>Password</Label>
								<Input
									type='password'
									name='password'
									id='password'
									placeholder='********'
									value={this.state.password}
									onChange={e => this.handleChange(e)}
								/>
							</FormGroup>

							<FormGroup>
								<Label>Confirm Password</Label>
								<Input
									type='password'
									name='confirmPassword'
									id='confirmPassword'
									placeholder='********'
									value={this.state.confirmPassword}
									onChange={e => this.handleChange(e)}
								/>
							</FormGroup>

							<FormGroup>
								<Label>Account Type</Label>
								<Select
									onSelect={this.handleSelectChange}
									size='large'
									// defaultValue='2'
								>
									<Option value='1'>Salon</Option>
									<Option value='2'>Freelancer Stylist/Educator</Option>
								</Select>
							</FormGroup>

							<Button
								type='submit'
								block
								color='primary'
								outline
								size='big'
								disabled={
									this.state.password !== this.state.confirmPassword
										? true
										: this.state.disable === true
										? true
										: false
								}
							>
								Register
							</Button>
						</Form>
					</Col>
				</div>
			</Container>
		);
	}
}

export default Signup;
