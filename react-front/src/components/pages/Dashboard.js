import React, { Component } from 'react';
// import { } from 'reactstrap';
import { Row, Col } from 'antd';
import MyButton from '../elements/Button';
import DashboardProfileSummary from '../elements/DashboardProfileSummary';
import Auth from '../../services/Auth';
import Axios from 'axios';
import Loader from '../elements/Loader';

class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = { isLoading: true };
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
	}

	componentWillMount() {
		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState(
					{
						currentUserId: res.data.userId,
						currentUser: res.data.username,
						currentUserRole: res.data.roleId
					},
					() => {
						if (this.state.currentUserRole === 1) {
							this.setState({ profiletype: 'salon' }, () => {
								Axios.get(
									'http://localhost:8000/api/bookings/countSalonBookings/' +
										this.state.currentUserId
								).then(res => {
									let noOfBookings = res.data.count;
									console.log('noOfBookings', noOfBookings);
									this.setState({ bookingCount: noOfBookings }, () => {
										Axios.get(
											'http://localhost:8000/api/salonUserInfo/' +
												this.state.currentUserId
										).then(res => {
											let salonInfo = res.data;
											let name = salonInfo.salonName;
											this.setState({ name: name });
											console.log(this.state);
											this.setState({ isLoading: false });
										});
									});
								});
							});
						} else if (this.state.currentUserRole === 2) {
							this.setState({ profiletype: 'freelancer' }, () => {
								Axios.get(
									'http://localhost:8000/api/bookings/countFreelancerBookings/' +
										this.state.currentUserId
								).then(res => {
									let noOfBookings = res.data.count;
									console.log('noOfBookings', noOfBookings);
									this.setState({ bookingCount: noOfBookings }, () => {
										Axios.get(
											'http://localhost:8000/api/freelancerUserInfo/' +
												this.state.currentUserId
										).then(res => {
											let freelancerInfo = res.data;
											let name =
												freelancerInfo.firstName +
												' ' +
												freelancerInfo.lastName;
											this.setState({ name: name });
											console.log(this.state);
											this.setState({ isLoading: false });
										});
									});
								});
							});
						}
					}
				);
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});
	}

	goToMyBookings = () => {
		this.props.history.push('/my-bookings');
	};

	goToDetails = () => {
		this.props.history.push({
			pathname: '/user-details',
			state: { currentUserId: this.state.currentUserId }
		});
	};

	render() {
		if (this.state.isLoading) {
			return <Loader />;
		} else {
			return (
				<div className='dashboard-container'>
					<Row>
						<Col span={10} offset={2}>
							<Row type='flex' justify='space-around' align='middle'>
								{/* <DashboardProfileSummary
                sessions='0'
                favs='2'
                rating='4'
                freelancerName='Veivesh'
                freelancerClass='Stylist'
			  /> */}

								<DashboardProfileSummary
									profiletype={this.state.profiletype}
									sessions={this.state.bookingCount}
									freelancerName={this.state.name}
									freelancerClass=''
									salonName={this.state.name}
									imageUrl='../../../assets/images/profile/pic-3.jpg'
								/>
							</Row>
						</Col>
						<Col span={10}>
							<Row type='flex' justify='center' align='top'>
								<Col>
									<MyButton
										buttontype='dashboard-button'
										onclick={this.goToMyBookings}
										name='My Bookings'
									/>
								</Col>
								<Col>
									<MyButton
										buttontype='dashboard-button'
										onclick={this.goToDetails}
										name='Edit My Profile'
									/>
								</Col>
							</Row>
							<Row
								type='flex'
								justify='center'
								align='top'
								hidden={this.state.currentUserRole === 1 ? true : false}
							>
								<Col>
									<MyButton
										// enable={this.state.userRoleId === 1}
										buttontype='dashboard-button'
										name='Customize Schedule'
									/>
								</Col>
								<Col>
									<MyButton
										// enable={this.state.userRoleId === 1}
										buttontype='dashboard-button'
										name='Update My Portfolio'
									/>
								</Col>
							</Row>
						</Col>
					</Row>
				</div>
			);
		}
	}
}

export default Dashboard;
