import React, { Component } from 'react';
import { Button, Calendar, Modal, Steps, message, Checkbox, Radio } from 'antd';
import { Container, Col, Row } from 'reactstrap';
import moment from 'moment';
import Axios from 'axios';
import Auth from '../../services/Auth';

class BookingPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			ModalText: '',
			visible: false,
			confirmLoading: false,
			title: 'Pick your preferred agenda',
			selectedSessionLevel: '',
			selectedSessionSlot: '',
			bool: false,
			bool2: false,
			bool3: false,
			bool4: false
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
	}

	componentDidMount() {
		Axios.get(
			'http://localhost:8000/api/freelancers/bookingDetails/' +
				this.state.freelancerId
		)
			.then(res => {
				let details = res.data;
				this.setState({ details });
				//console.log(this.state);
			})
			.catch(err => {
				//console.error(err);
			});
	}

	componentWillMount() {
		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				//console.log(this.state);
			})
			.catch(err => {
				//console.error(err);
			});

		let freelancerId = Number(this.props.match.params.id);
		this.setState({ freelancerId }, () => {
			//console.log('freelancerId', this.state.freelancerId);
			Axios.get(
				'http://localhost:8000/api/freelancers/' + this.state.freelancerId
			)
				.then(res => {
					// console.log(res);
					this.setState({
						currentUserLevelId: res.data.levelId
					});
				})
				.catch(err => {
					//console.error(err);
				});
		});
	}

	showModal = value => {
		// let valuez = moment(value);

		let selectedDate = value.format('YYYY-MM-DD');
		Axios.post('http://localhost:8000/api/bookings/search', {
			freelancerId: this.state.freelancerId,
			bookingDate: selectedDate
		})
			.then(res => {
				// console.log(res);
				let currentBookings = res.data;
				this.setState({ currentBookings }, () => {
					this.state.currentBookings.map((booking, i) => {
						if (booking.status !== 'cancelled') {
							if (booking.slotId === 1) {
								this.setState({ bool: true });
							} else if (booking.slotId === 2) {
								this.setState({ bool2: true });
							}
						}
					});
				});
			})
			.catch(err => {
				console.error(err);
			});
		this.setState(
			{
				selectedDate: selectedDate,
				ModalText: `Do you want to book for a session on ${selectedDate}?`,
				title: `Pick your preferred agenda for ${selectedDate}`,
				bool: false,
				bool2: false,
				bool3: false,
				bool4: false
			},
			() => {
				this.setState({ visible: true });
			}
		);
	};

	handleOk = () => {
		this.setState(
			{
				ModalText: `Saving your preferences... Please wait.`,
				confirmLoading: true
			},
			() => {
				let currentState = this.state;
				Axios.post('http://localhost:8000/api/bookings', currentState)
					.then(res => {
						console.log(res);
						this.setState({
							visible: false,
							confirmLoading: false
						});

						this.setState({
							selectedSessionLevel: null,
							selectedSessionSlot: null
						});
					})
					.catch(err => {
						console.error(err);
					});

				console.log(this.state);
			}
		);
	};

	handleCancel = () => {
		//console.log('Clicked cancel button');
		this.setState({
			visible: false
		});
	};

	navigate = () => {
		this.props.history.push({
			pathname: `/book/confirm-booking/${this.state.freelancerId}`
		});
	};

	handleSessionLevel = e => {
		// console.log(e);
		this.setState({ selectedSessionLevel: e.target.value });
	};
	handleSessionSlot = e => {
		this.setState({ selectedSessionSlot: e.target.value });
	};

	render() {
		const { Step } = Steps;
		const { visible, confirmLoading, ModalText } = this.state;
		return (
			<Container className='booking-container'>
				<Row>
					<div style={{ padding: '0px 10rem', width: '100%' }}>
						<Steps progressDot current={0} size='small'>
							<Step title='Pick a slot' />
							<Step title='Confirm booking' />
						</Steps>
					</div>
					<div className='steps-content'>
						<Calendar
							onChange={this.showModal}
							mode='month'
							validRange={[moment().add(1, 'd'), moment().add(7, 'days')]}
						/>
						<Modal
							centered
							title={this.state.title}
							visible={visible}
							onOk={this.handleOk}
							confirmLoading={confirmLoading}
							onCancel={this.handleCancel}
						>
							<div style={{ textAlign: 'center' }}>
								<h4 hidden={this.state.confirmLoading === true ? false : true}>
									{ModalText}
								</h4>

								<Row hidden={this.state.confirmLoading === true ? true : false}>
									<Col>
										<section>
											<h6>Select a slot</h6>
											<Radio.Group
												value={this.state.selectedSessionSlot}
												buttonStyle='solid'
												onChange={this.handleSessionSlot}
											>
												<Radio.Button value='1' disabled={this.state.bool}>
													Day
												</Radio.Button>
												<Radio.Button value='2' disabled={this.state.bool2}>
													Evening
												</Radio.Button>
											</Radio.Group>
										</section>
									</Col>
									<Col>
										<section>
											<h6>Preferred Service</h6>
											<Radio.Group
												value={this.state.selectedSessionLevel}
												buttonStyle='solid'
												onChange={this.handleSessionLevel}
											>
												<Radio.Button value='1' disabled={this.state.bool3}>
													Stylist
												</Radio.Button>
												<Radio.Button
													value='2'
													disabled={
														this.state.currentUserLevelId === 1
															? true
															: this.state.bool4
															? true
															: false
													}
												>
													Educator
												</Radio.Button>
											</Radio.Group>
										</section>
									</Col>
								</Row>
							</div>
						</Modal>
					</div>
					<div className='float-right'>
						<Button type='primary' onClick={this.navigate}>
							Continue
						</Button>
					</div>
				</Row>
			</Container>
		);
	}
}

export default BookingPage;
