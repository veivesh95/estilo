import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Nav,
	NavItem,
	NavLink,
	Collapse,
	Navbar,
	NavbarBrand,
	NavbarToggler
} from 'reactstrap';
import Footer from '../elements/Footer';
import Typed from 'react-typed';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Image from '../../assets/images/bg/salon-bg.jpg';
// import {Layout} from 'antd'
// import { NavLink } from 'react-router-dom';

let typeStrings = [
	'Find the best stylist in town',
	'Find a skilled expert to teach style tips',
	'Enhance your skills',
	'Boost your business'
];

let imageUrl = '../../assets/images/bg/salon-bg.jpg';
let styles = {
	width: '100%',
	height: '100%',
	backgroundImage:
		'linear-gradient(to right, rgb(11, 11, 11), #00000075), url(' + Image + ')',
	backgroundSize: 'cover',
	backgroundRepeat: 'no-repeat'
};

class Welcome extends Component {
	constructor(props) {
		super(props);
		console.log('Constructor');

		this.state = {
			role: '',
			isOpen: true
		};
	}

	componentDidMount() {
		console.log('ComponentDidMount');
	}

	handleChange = event => {
		event.preventDefault();

		let selected = event.target.value;
		let query = '';
		if (selected === 1) {
			query = 'stylist';
		} else if (selected === 2) {
			query = 'educator';
		}
		this.props.history.push({
			pathname: '/search',
			search: `?query=${query}`,
			state: { selectedRole: selected }
		});

		console.log(event.target.value);
	};

	render = props => {
		return (
			<div className='background' style={styles}>
				<Navbar
					expand='md'
					style={{ boxShadow: 'none' }}
					className='welcome-nav'
				>
					{/* <NavbarBrand> */}
					<NavItem
						className='navbar-brand'
						style={{
							color: 'white',
							fontWeight: 'bold',
							fontSize: '32px',
							cursor: 'arrow'
						}}
					>
						estilo.
					</NavItem>
					{/* </NavbarBrand> */}
					<NavbarToggler onClick={this.toggle} />
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className='ml-auto' navbar>
							<NavItem style={{ color: 'white' }}>
								<NavLink
									style={{ color: 'white', fontWeight: 'bold' }}
									href='/login'
								>
									Login
								</NavLink>
							</NavItem>
							<NavItem
								style={{
									color: 'white',
									border: '1px solid #fff',
									borderRadius: '5px'
								}}
							>
								<NavLink
									style={{ color: 'white', fontWeight: 'bold' }}
									href='/signup'
								>
									Join us
								</NavLink>
							</NavItem>
						</Nav>
					</Collapse>
				</Navbar>
				{/* <div className='background'> */}
				<Container>
					<Row>
						<div className='welcome-search-box'>
							<Col md={{ size: 10 }} style={{ height: '175px' }}>
								<h1 className='app-tagline'>
									Find the best in town
									{/* <Typed
										strings={typeStrings}
										typeSpeed={80}
										backSpeed={60}
										loop
									/> */}
									<span className='sub-tagline'> with estilo.</span>
								</h1>
							</Col>
							<br />
							<Col md={{ size: 8 }}>
								<h4 className='select-tagline'>Find a...</h4>
								<Select
									fullWidth
									value={this.state.role}
									onChange={this.handleChange}
									placeholder='Pick your preference'
									className='welcome-select'
								>
									<MenuItem value={1}>Stylist</MenuItem>
									<MenuItem value={2}>Educator</MenuItem>
								</Select>
								<h4 className='select-tagline'>for me. </h4>
							</Col>
						</div>
					</Row>
				</Container>
				<Footer />
			</div>
		);
	};
}

export default Welcome;
