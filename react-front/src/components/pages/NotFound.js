import React from 'react';

const NotFound = () => {
  return (
    <div className="notFoundContainer">
      <div>
        <h1 className="nf-title">404</h1>
        <h4 className="nf-caption">
          You underestimate the power of dark side!
        </h4>
      </div>
    </div>
  );
};

export default NotFound;
