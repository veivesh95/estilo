import React, { Component } from 'react';
import Axios from 'axios';
import Auth from '../../services/Auth';
import { Container, Row, Col } from 'reactstrap';
import MyCard from '../elements/Card';
import { array } from 'prop-types';
import Loader from '../elements/Loader';

class MyBookings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
		this.cancelBooking = this.cancelBooking.bind(this);
		this.completeBooking = this.completeBooking.bind(this);
		this.changeStatus = this.changeStatus.bind(this);
	}

	async componentWillMount() {
		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				let currentUserId = res.data.userId;
				this.setState(
					{
						currentUserId: res.data.userId,
						currentUser: res.data.username,
						currentUserRole: res.data.roleId
					},
					() => {
						console.log(this.state);
						if (this.state.currentUserRole === 1) {
							Axios.get(
								`http://localhost:8000/api/bookings/myBookings/${currentUserId}`
							).then(res => {
								let bookings = res.data.bookings;
								if (bookings.length === 0) {
									this.setState({ newUser: true, isLoading: false });
								} else {
									this.setState({ bookingList: bookings }, () => {
										this.setState({ isLoading: false });
										console.log(this.state);
									});
								}
							});
						} else {
							Axios.get(
								`http://localhost:8000/api/bookings/myBookings2/${currentUserId}`
							).then(res => {
								let bookings = res.data.bookings;
								if (bookings.length === 0) {
									this.setState({ newUser: true, isLoading: false });
								} else {
									this.setState({ bookingList: bookings }, () => {
										this.setState({ isLoading: false });
										console.log(this.state);
									});
								}
							});
						}
					}
				);
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});
	}

	completeBooking(e) {
		e.preventDefault();
		console.log();
	}

	cancelBooking(e) {
		e.preventDefault();
		console.log(e);
	}

	changeStatus = e => {
		// e.preventDefault();
		console.log(e);
	};

	render() {
		if (this.state.isLoading) {
			return <Loader />;
		}

		if (this.state.newUser) {
			return (
				<div className='text-center'>
					<h2>You don't have any bookings</h2>
				</div>
			);
		}
		return (
			<Container className='my-booking-container'>
				<Row>
					<div>
						<h4 className='text-center'>My Bookings</h4>
					</div>
				</Row>
				<Row>
					<div className='cards-container' style={{ width: '100%' }}>
						{this.state.bookingList.map((booking, i) => {
							return (
								<MyCard
									onClick={this.changeStatus}
									cardtype='bookings'
									key={booking.bookingId}
									bookingId={booking.bookingId}
									bookingDate={booking.bookingDate}
									createdDate={booking.createdDate}
									firstName={booking.firstName}
									lastName={booking.lastName}
									amount={booking.amount}
									status={booking.status}
									slotId={booking.slotId}
									levelId={booking.levelId}
									change={this.changeStatus}
								/>
							);
						})}
					</div>
				</Row>
			</Container>
		);
	}
}

export default MyBookings;
