import React, { Component } from 'react';
import {
	Container,
	Col,
	Row,
	Form,
	FormGroup,
	FormText,
	FormFeedback,
	Label,
	Input
} from 'reactstrap';
import { Button } from 'antd';
import MyFormGroup from '../elements/FormGroup';
import Auth from '../../services/Auth';
import Axios from 'axios';

class SalonDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			disable: true
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
	}

	componentWillMount() {
		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});
	}

	handleChange = async event => {
		const { target } = event;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const { name } = target;
		this.setState({
			[name]: value
		});
	};

	submitForm(e) {
		e.preventDefault();
		let { currentUserId } = this.state;
		let salonDetails = this.state;
		console.log(this.state);
		// console.log(this.freelancerId);

		Axios.put('http://localhost:8000/api/salons/' + currentUserId, salonDetails)
			.then(res => {
				let salonId = res.data.salonId;
				if (res.status === 200 && this.state.currentUserRole === 1) {
					this.props.history.push({
						pathname: '/dashboard',
						state: { salonId: salonId, userRoleId: this.state.currentUserRole }
					});
				}
			})
			.catch(err => console.error(err));

		// let pathLink = this.props.match.path;
		// if (pathLink === '/salon/details') {
		// 	this.props.history.push('/imageUpload');
		// } else if (pathLink === '/freelancer/details') {
		// 	this.props.history.push('/updateSkills');
		// }
	}

	handleMaxLength = e => {
		var len = this.state.salonEIN;
		if (String(len).length > 9) {
			this.setState({ disable: true });
		} else {
			this.setState({ disable: false });
		}
	};
	getEin = value => {
		// const { value } = event.target;
		this.setState({ salonEIN: value });
	};
	handleContactLength = e => {
		var len = this.state.salonContact;

		if (String(len).length > 9) {
			this.setState({ disable: true });
		} else {
			this.setState({ disable: false });
		}
	};

	render() {
		return (
			<Container className='details-container'>
				<Col md={{ size: 10, offset: 1 }}>
					<Form className='form' onSubmit={e => this.submitForm(e)}>
						<section>
							<Row>
								<Col md={{ size: 4 }}>
									<h5 className='text-right'>Business Details</h5>
								</Col>
								<Col md={{ size: 8 }}>
									{/* <MyFormGroup
                                placeholder='Abc Salon'
                                name='salon'
                                formgrouptype='normal'
                                label='Salon'
                                value={this.state.salon}
                                changemethod={this.handleChange}
                              /> */}
									<MyFormGroup
										placeholder='Abc Salon'
										name='salonName'
										formgrouptype='normal'
										label='Salon'
										value={this.state.salonName}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='Street Number'
										name='salonStreetNumber'
										formgrouptype='normal'
										label='Street Number'
										value={this.state.salonStreetNumber}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='Street'
										name='salonStreet'
										formgrouptype='normal'
										label='Street'
										value={this.state.salonStreet}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='City'
										name='salonCity'
										formgrouptype='normal'
										label='City'
										value={this.state.salonCity}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='State Code'
										name='salonStateCode'
										formgrouptype='normal'
										label='State Code'
										pattern='[A-Z]{3}'
										title='3 Letter code'
										value={this.state.salonStateCode}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='ZIP'
										name='salonZip'
										formgrouptype='normal'
										label='Zip'
										value={this.state.salonZip}
										changemethod={this.handleChange}
									/>

									<MyFormGroup
										placeholder='Employee Identification Number'
										name='salonEIN'
										type='number'
										formgrouptype='ein'
										label='EIN'
										length={9}
										value={this.state.salonEIN}
										changemethod={e => {
											this.handleMaxLength(e);
											this.getEin(e);
										}}
									/>
									<MyFormGroup
										placeholder='00112233445566'
										name='salonContact'
										formgrouptype='normal'
										label='Business Contact'
										value={this.state.salonContact}
										changemethod={this.handleChange}
									/>
								</Col>
							</Row>
						</section>
						<div className='float-right'>
							<Button
								type='primary'
								disabled={this.state.disable}
								htmlType='submit'
							>
								Continue
							</Button>
						</div>

						{/* <Button>Submit</Button> */}
					</Form>
				</Col>
			</Container>
		);
	}
}

export default SalonDetails;
