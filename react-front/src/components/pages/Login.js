import React, { Component } from 'react';
import {
	Container,
	Col,
	Form,
	FormGroup,
	FormText,
	FormFeedback,
	Label,
	Input,
	Button
} from 'reactstrap';
import Auth from '../../services/Auth';
import Axios from 'axios';
import { notification } from 'antd';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: 'asdf@asdf.com',
			password: 'asdfasdf',
			validate: {
				emailState: ''
			}
		};
		this.handleChange = this.handleChange.bind(this);
		this.auth = new Auth();
	}

	componentWillMount() {
		this.setState({ hideNav: true });
	}

	validateEmail(e) {
		const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const { validate } = this.state;
		if (emailRex.test(e.target.value)) {
			validate.emailState = 'has-success';
		} else {
			validate.emailState = 'has-danger';
		}
		this.setState({ validate });
	}

	handleChange = async event => {
		const { target } = event;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const { name } = target;
		await this.setState({
			[name]: value
		});
	};

	componentWillMount() {
		if (this.auth.isLoggedIn()) {
			this.props.history.replace('/');
		}
	}

	submitForm = e => {
		e.preventDefault();
		console.log(this.state);

		Axios.post('http://localhost:8000/api/login', {
			username: this.state.email,
			password: this.state.password
		})
			.then(res => {
				this.auth.setToken(res.data.token);
				console.log(res.data.token);
				notification.success({
					message: 'Login Successful',
					onClick: () => {
						console.log('Notification Clicked!');
					}
				});
				this.props.history.replace('/dashboard');
			})
			.catch(err => {
				notification.error({
					message: 'Login Failed',
					description:
						'Are you certain that you have given the valid credentials? Please try again.',
					onClick: () => {
						console.log('Notification Clicked!');
					}
				});
				console.error(err.response);
			});

		// this.props.history.push('/dashboard');
	};

	render() {
		return (
			<Container>
				<div className='login-container'>
					<Col md={{ offset: 4, size: 4 }}>
						<h2>
							Login to <strong>estilo</strong>
						</h2>
						<Form className='form' onSubmit={this.submitForm}>
							<FormGroup>
								<Label>Username</Label>
								<Input
									type='email'
									name='email'
									id='exampleEmail'
									placeholder='myemail@email.com'
									value={this.state.email}
									valid={this.state.validate.emailState === 'has-success'}
									invalid={this.state.validate.emailState === 'has-danger'}
									onChange={e => {
										this.validateEmail(e);
										this.handleChange(e);
									}}
								/>
								<FormFeedback valid>
									{/* That's a tasty looking email you've got there. */}
								</FormFeedback>
								<FormFeedback>
									{/* Uh oh! Looks like there is an issue with your email. Please
									input a correct email. */}
								</FormFeedback>
								{/* <FormText>Your username is most likely your email.</FormText> */}
							</FormGroup>

							<FormGroup>
								<Label for='examplePassword'>Password</Label>
								<Input
									type='password'
									name='password'
									id='examplePassword'
									placeholder='********'
									value={this.state.password}
									onChange={e => this.handleChange(e)}
								/>
							</FormGroup>

							<Button type='submit' block outline size='big'>
								Login
							</Button>
						</Form>
					</Col>
				</div>
			</Container>
		);
	}
}

// const Login = () => {
//   return (
//     <div>
//       <br />

//       <Input inputtype='' type='text' name='name' placeholder='Veivesh' />
//     </div>
//   );
// };

export default Login;
