import React, { Component } from 'react';
import { Container, Row, Col, Form, FormGroup, Label } from 'reactstrap';
import { Select, DatePicker, Slider } from 'antd';
import MyFormGroup from '../elements/FormGroup';
import SearchResult from '../elements/SearchResult';
import Auth from '../../services/Auth';
import Axios from 'axios';
import moment from 'moment';
import ReactLoading from 'react-loading';
import Loader from '../elements/Loader';

// const Example = ({ type, color }) => {false
// 	<ReactLoading type={type} color={color} height={'20%'} width={'20%'} />;
// };

class Search extends Component {
	constructor(props) {
		super(props);
		this.select1 = React.createRef();
		this.state = {
			freelancerList: [],
			isLoading: true
			// selectedRole: 1
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
		this.goToProfile = this.goToProfile.bind(this);
	}

	async componentWillMount() {
		// Getting user details from token
		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});

		if (this.props.location.state) {
			let selectedRole = this.props.location.state.selectedRole;
			this.setState({ selectedRole: selectedRole, fromHome: true });
		} else {
			this.setState({ selectedRole: 1, fromHome: false });
		}

		// Axios.get(
		// 	'localhost:8000/api/freelancers/searchByLevel/' + this.state.selectedRole
		// ).then(res => {
		// 	let filteredFreelancers = res.data.freelancers;
		// 	this.setState({ filteredFreelancerList: filteredFreelancers }, () => {});
		// });
		// this.freelancerList = freelancers.map(function(freelancer) {
		// 	return <SearchResult key={freelancer.freelancerId} />;
		// });
	}

	async componentDidMount() {
		if (this.state.fromHome) {
			Axios.get(
				`http://localhost:8000/api/freelancers/searchByLevel/${
					this.state.selectedRole
				}`
			).then(res => {
				let filteredFreelancers = res.data.freelancers;
				this.setState({ filteredFreelancerList: filteredFreelancers }, () => {
					this.setState({ isLoading: false });
				});
			});
		} else {
			Axios.get('http://localhost:8000/api/freelancers123').then(res => {
				let filteredFreelancers = res.data.freelancers;
				this.setState({ filteredFreelancerList: filteredFreelancers }, () => {
					// this.freelancerList = this.state.freelancers;
					this.setState({ selectedRole: null, isLoading: false });
				});
				console.log(this.state);
			});
		}

		// Axios.get(
		// 	`http://localhost:8000/api/freelancers/searchByLevel/${
		// 		this.state.selectedRole
		// 	}`
		// ).then(res => {
		// 	let filteredFreelancers = res.data.freelancers;
		// 	this.setState({ filteredFreelancerList: filteredFreelancers }, () => {
		// 		this.setState({ isLoading: false });
		// 	});
		// });
	}

	handleType = value => {
		console.log(value);
		this.setState({ selectedRole: value, isLoading: true }, () => {
			Axios.get(
				`http://localhost:8000/api/freelancers/searchByLevel/${
					this.state.selectedRole
				}`
			).then(res => {
				let filteredFreelancers = res.data.freelancers;
				this.setState({ filteredFreelancerList: filteredFreelancers }, () => {
					this.setState({ isLoading: false });
				});
			});
		});
	};

	handleRating = value => {
		this.setState({ rating: value, isLoading: true }, () => {
			Axios.post(
				'http://localhost:8000/api/freelancers/searchByLevelAndRating',
				{ levelId: this.state.selectedRole, rating: this.state.rating }
			).then(res => {
				let filteredFreelancers = res.data.freelancers;
				this.setState({ filteredFreelancerList: filteredFreelancers }, () => {
					this.setState({ isLoading: false });
				});
			});
		});
		console.log(Number(value));
	};

	handleDate = value => {
		let date = value.format('YYYY-MM-DD');
		this.setState({ filteredFreelancerList: null, isLoading: true }, () => {
			this.setState({ date: String(date) }, () => {
				Axios.post('http://localhost:8000/api/freelancers/searchByDate', {
					date: this.state.date
				}).then(res => {
					console.log(res.data);

					let filteredFreelancers = res.data.freelancers[0];
					this.setState({ filteredFreelancerList: filteredFreelancers }, () => {
						this.setState({ isLoading: false });
					});
				});
			});
		});

		console.log(date);

		// this.setState({ date: value }, () => {
		//   console.log(this.state);
		// });
	};

	handleFeeChange = value => {
		// console.log(value);
	};

	afterFeeChange = value => {
		console.log(value);
	};

	detailsOf = e => {
		console.log(e);
	};

	goToProfile(e) {
		const to = { pathname: `/profile/${e}` };
		this.props.history.push(to);
	}
	disabledDate(current) {
		return current && current < moment().endOf('day');
	}

	render() {
		let Option = Select.Option;
		let { WeekPicker } = DatePicker;
		const { isLoading, fromHome } = this.state;

		if (isLoading) {
			return <Loader />;
		} else {
			if (fromHome) {
				return (
					<Container className='search-container'>
						<Row>
							<Col md={{ size: 4 }}>
								<div className='filter-container'>
									<h4>Narrow your results</h4>
									<br />
									<Form>
										<FormGroup>
											<Label>Freelancer Type</Label>
											<Select
												onChange={this.handleType}
												value={
													this.state.selectedRole === 1
														? 1
														: this.state.selectedRole === null
														? ''
														: 2
												}
											>
												<Option value={1}>Stylists</Option>
												<Option value={2}>Educator</Option>
											</Select>
										</FormGroup>
										<FormGroup>
											<Label>Date</Label>
											<br />
											<WeekPicker
												format='YYYY-MM-DD'
												// value={current}

												disabledDate={this.disabledDate}
												style={{ width: '100%' }}
												onChange={this.handleDate}
												placeholder='Select week'
											/>
										</FormGroup>
										<FormGroup hidden>
											<Label>Fee Rate</Label>
											<Slider
												range
												step={10}
												defaultValue={[250, 500]}
												min={100}
												max={1500}
												onChange={this.handleFeeChange}
												onAfterChange={this.afterFeeChange}
											/>
										</FormGroup>
										<FormGroup hidden>
											<Label>Rating</Label>
											<Select onChange={this.handleRating}>
												<Option value='4'>4 or above</Option>
												<Option value='3'>3 or above</Option>
												<Option value='2'>2 or above</Option>
												<Option value='1'>1 or above</Option>
											</Select>
										</FormGroup>
										<FormGroup hidden>
											<Label>Skill</Label>
											<Select onChange={this.handleType}>
												<Option value='stylist'>Stylists</Option>
												<Option value='educator'>Educator</Option>
											</Select>
										</FormGroup>
									</Form>
								</div>
							</Col>
							<Col md={{ size: 8 }}>
								<div className='results-container'>
									<div>
										<div>
											{this.state.filteredFreelancerList.map(
												(freelancer, i) => {
													return (
														<SearchResult
															key={freelancer.freelancerId}
															firstName={freelancer.firstName}
															lastName={freelancer.lastName}
															onclick={() =>
																this.goToProfile(freelancer.freelancerId)
															}
														/>
													);
													// <li key={idx}>{d.name}</li>;
												}
											)}
										</div>
										{/* {this.state.freelancers.map(function(freelancer, index) {
									return (
										<div key={index}>
											<li>{freelancer.username}</li>
										</div>
									);
								})} */}
										{/* {this.state.freelancers.map(freelancer => (
									<SearchResult
										key={freelancer.freelancerId}
										onClick={this.goToProfile}
									/>
								))} */}
										{/* <div className='movies'>
									{Object.keys(this.state.freelancers).map(freelancer => (
										<SearchResult key={freelancer.freelancerId} />
									))}
								</div> */}
									</div>
									{/* <SearchResult onclick={this.goToProfile} />
							<SearchResult onclick={this.goToProfile} />
							<SearchResult onclick={this.goToProfile} /> */}
								</div>
							</Col>
						</Row>
					</Container>
				);
			} else {
				return (
					<Container className='search-container'>
						<Row>
							<Col md={{ size: 4 }}>
								<div className='filter-container'>
									<h4>Narrow your results</h4>
									<br />
									<Form>
										<FormGroup>
											<Label>Freelancer Type</Label>
											<Select
												onChange={this.handleType}
												value={
													this.state.selectedRole === 1
														? 1
														: this.state.selectedRole === null
														? ''
														: 2
												}
											>
												<Option value={1}>Stylists</Option>
												<Option value={2}>Educator</Option>
											</Select>
										</FormGroup>
										<FormGroup>
											<Label>Date</Label>
											<br />
											<WeekPicker
												style={{ width: '100%' }}
												// dateRender={current => {
												//   const style = {};
												//   if (current.date() === 1) {
												//     style.border = '1px solid #1890ff';
												//     style.borderRadius = '50%';
												//   }
												//   return (
												//     <div className='ant-calendar-date' style={style}>
												//       {current.date()}
												//     </div>
												//   );
												// }}
												onChange={this.handleDate}
												placeholder='Select week'
											/>
										</FormGroup>
										<FormGroup hidden>
											<Label>Fee Rate</Label>
											<Slider
												range
												step={10}
												defaultValue={[250, 500]}
												min={100}
												max={1500}
												onChange={this.handleFeeChange}
												onAfterChange={this.afterFeeChange}
											/>
										</FormGroup>
										<FormGroup hidden>
											<Label>Rating</Label>
											<Select onChange={this.handleRating}>
												<Option value='4'>4 or above</Option>
												<Option value='3'>3 or above</Option>
												<Option value='2'>2 or above</Option>
												<Option value='1'>1 or above</Option>
											</Select>
										</FormGroup>
										<FormGroup hidden>
											<Label>Skill</Label>
											<Select onChange={this.handleType}>
												<Option value='stylist'>Stylists</Option>
												<Option value='educator'>Educator</Option>
											</Select>
										</FormGroup>
									</Form>
								</div>
							</Col>
							<Col md={{ size: 8 }}>
								<div className='results-container'>
									<div>
										<div>
											{this.state.filteredFreelancerList.map(
												(freelancer, i) => {
													return (
														<SearchResult
															key={freelancer.freelancerId}
															firstName={freelancer.firstName}
															lastName={freelancer.lastName}
															onclick={() =>
																this.goToProfile(freelancer.freelancerId)
															}
														/>
													);
													// <li key={idx}>{d.name}</li>;
												}
											)}
										</div>
										{/* {this.state.freelancers.map(function(freelancer, index) {
											return (
												<div key={index}>
													<li>{freelancer.username}</li>
												</div>
											);
										})} */}
										{/* {this.state.freelancers.map(freelancer => (
											<SearchResult
												key={freelancer.freelancerId}
												onClick={this.goToProfile}
											/>
										))} */}
										{/* <div className='movies'>
											{Object.keys(this.state.freelancers).map(freelancer => (
												<SearchResult key={freelancer.freelancerId} />
											))}
										</div> */}
									</div>
									{/* <SearchResult onclick={this.goToProfile} />
									<SearchResult onclick={this.goToProfile} />
									<SearchResult onclick={this.goToProfile} /> */}
								</div>
							</Col>
						</Row>
					</Container>
				);
			}
		}
	}
}

export default Search;
