import React, { Component } from 'react';
import {
	Container,
	Col,
	Row,
	Form,
	FormGroup,
	FormText,
	FormFeedback,
	Label,
	Input
} from 'reactstrap';
import { Button } from 'antd';
import MyFormGroup from '../elements/FormGroup';
import Auth from '../../services/Auth';
import Axios from 'axios';

import Yup from 'yup';

// const detailsSchema = Yup.object().shape({
// 	firstName: Yup.string().required(),
// 	lastName: Yup.string().required(),
// 	userEin: Yup.string()
// });

class Details extends Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmDirty: false,
			disable: true,
			firstName: '',
			lastName: '',
			userEin: '',
			contact: '',
			contactWork: '',
			streetNumber: '',
			street: '',
			city: '',
			stateCode: '',
			zip: ''
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();

		this.handleChange = this.handleChange.bind(this);
		this.handleConfirmBlur = this.handleConfirmBlur.bind(this);
		this.handleMaxLength = this.handleMaxLength.bind(this);
	}

	componentWillMount() {
		let pathLink = this.props.match.path;
		// let response = this.auth.getUser();
		// console.log('response', response)

		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});

		// this.user = this.auth.checkUser();

		// console.log(this.auth.checkUser());

		// console.log('link', link);
		// if (pathLink === '/salon/details') {
		// 	this.render = this.salonForm;
		// } else if (pathLink === '/freelancer/details') {
		// 	this.render = this.freelancerForm;
		// }
	}

	getEin = value => {
		// const { value } = event.target;
		this.setState({ userEin: value });
	};

	handleChange = async event => {
		const { target } = event;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const { name } = target;
		this.setState({
			[name]: value
		});
	};

	handleConfirmBlur = e => {
		const value = e.target.value;
		this.setState({ confirmDirty: this.state.confirmDirty || !!value });
	};

	handleMaxLength = e => {
		var len = this.state.userEin;
		if (String(len).length > 9) {
			this.setState({ disable: true });
		} else {
			this.setState({ disable: false });
		}
	};

	handleContactLength = e => {
		var len = this.state.contact;
		var len2 = this.state.contactWork;

		if (String(len).length > 9 || String(len2).length > 9) {
			this.setState({ disable: true });
		} else {
			this.setState({ disable: false });
		}
	};

	submitForm(e) {
		e.preventDefault();
		let { currentUserId } = this.state;
		let userDetails = this.state;
		console.log(this.state);
		// console.log(this.freelancerId);

		Axios.put('http://localhost:8000/api/user/' + currentUserId, userDetails)
			.then(res => {
				if (res.status === 200) {
					this.props.history.push({
						pathname: '/signup/image-upload'
					});
				}
				// if (res.status === 200 && this.state.currentUserRole === 2) {
				// 	this.props.history.push({
				// 		pathname: '/signup/update-skills'
				// 	});
				// } else if (res.status === 200 && this.state.currentUserRole === 1) {
				// 	this.props.history.push('/signup/salon-details');
				// }
				// console.log(res);
			})
			.catch(err => console.error(err));

		// let pathLink = this.props.match.path;
		// if (pathLink === '/salon/details') {
		// 	this.props.history.push('/imageUpload');
		// } else if (pathLink === '/freelancer/details') {
		// 	this.props.history.push('/updateSkills');
		// }
	}

	render() {
		const {
			firstName,
			lastName,
			userEin,
			contact,
			contactWork,
			streetNumber,
			street,
			city,
			stateCode,
			zip,
			disable
		} = this.state;

		return (
			<Container className='details-container'>
				<Col md={{ size: 10, offset: 1 }}>
					<Form className='form' onSubmit={e => this.submitForm(e)}>
						<section>
							<Row>
								<Col md={{ size: 4 }}>
									<h5 className='text-right'>Tell us a bit about you</h5>
								</Col>
								<Col md={{ size: 8 }}>
									{/* First Name */}
									<MyFormGroup
										focus={true}
										placeholder='John'
										name='firstName'
										formgrouptype='normal'
										label='First Name'
										type='text'
										pattern='^[A-Za-z -]+$'
										title='Your first name in valid format please'
										value={this.state.firstName}
										changemethod={this.handleChange}
									/>

									{/* Last Name */}
									<MyFormGroup
										placeholder='Doe'
										name='lastName'
										type='text'
										pattern='^[A-Za-z -]+$'
										title='Your last name in valid format please'
										formgrouptype='normal'
										label='Last Name'
										value={this.state.lastName}
										changemethod={this.handleChange}
									/>
									{/* EIN */}
									<MyFormGroup
										placeholder='123456789'
										name='userEin'
										length={9}
										formgrouptype='ein'
										type='number'
										label='Employee Identitfication Number (EIN)'
										// pattern='^(\d{9})$'
										value={this.state.userEin}
										changemethod={e => {
											this.handleMaxLength(e);
											this.getEin(e);
										}}
									/>

									<Row>
										<Col>
											{/* businessContact */}
											<MyFormGroup
												placeholder='001122334455'
												name='contact'
												type='number'
												formgrouptype='normal'
												label='Contact (Mobile)'
												value={this.state.contact}
												changemethod={e => {
													this.handleContactLength(e);
													this.handleChange(e);
												}}
											/>
										</Col>

										<Col>
											{/* businessContact */}
											<MyFormGroup
												placeholder='001122334455'
												name='contactWork'
												formgrouptype='normal'
												label='Contact (Work)'
												type='number'
												value={this.state.contactWork}
												changemethod={e => {
													this.handleContactLength(e);
													this.handleChange(e);
												}}
											/>
										</Col>
									</Row>
								</Col>
							</Row>
						</section>
						<hr />
						<section>
							<Row>
								<Col md={{ size: 4 }}>
									<h5 className='text-right'>Personal Details</h5>
								</Col>
								<Col md={{ size: 8 }}>
									<MyFormGroup
										placeholder='Street Number'
										name='streetNumber'
										formgrouptype='normal'
										type='text'
										label='Street Number'
										value={this.state.streetNumber}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='Street'
										name='street'
										formgrouptype='normal'
										label='Street'
										type='text'
										value={this.state.street}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='City'
										name='city'
										type='text'
										formgrouptype='normal'
										label='City'
										value={this.state.city}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='State Code'
										name='stateCode'
										type='text'
										formgrouptype='normal'
										label='State Code'
										pattern='[A-Z]{3}'
										title='3 Letter code'
										value={this.state.stateCode}
										changemethod={this.handleChange}
									/>
									<MyFormGroup
										placeholder='ZIP'
										name='zip'
										type='number'
										formgrouptype='normal'
										label='Zip'
										value={this.state.zip}
										changemethod={this.handleChange}
									/>
								</Col>
							</Row>
						</section>
						<div className='float-right'>
							<Button
								type='primary'
								disabled={this.state.disable}
								htmlType='submit'
							>
								Continue
							</Button>
						</div>
					</Form>
				</Col>
			</Container>
		);
	}
}

export default Details;
