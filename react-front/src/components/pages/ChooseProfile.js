import React, { Component } from 'react';
// import { Button } from 'antd';
import MyButton from '../elements/Button';

class ChooseProfile extends Component {
  constructor(props) {
    super(props);

    this.changeRoute = this.changeRoute.bind(this);
    this.changeRoute2 = this.changeRoute2.bind(this);
  }

  changeRoute(e) {
    e.preventDefault();
    this.props.history.push('/freelancer/details');
  }

  changeRoute2(e) {
    e.preventDefault();
    this.props.history.push('/salon/details');
  }

  render() {
    return (
      <div className='picker-container'>
        <div className='picker-button-group'>
          <MyButton
            buttontype='normal'
            name='Stylists and Educators'
            onClick={this.changeRoute}
          />
          <div className='vertical-border' />
          <MyButton
            buttontype='normal'
            name='Salon Owners'
            onClick={this.changeRoute2}
          />
        </div>
      </div>
    );
  }
}

export default ChooseProfile;
