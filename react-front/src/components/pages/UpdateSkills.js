import React, { Component } from 'react';
import { Col, Row, Container, Form } from 'reactstrap';
import { Button, Radio, Checkbox } from 'antd';
import MyFormGroup from '../elements/FormGroup';
import Axios from 'axios';
import Auth from '../../services/Auth';

const CheckboxGroup = Checkbox.Group;
const menOptions = [
	'Advanced',
	'Classics',
	'Shaving',
	'Crew Cut',
	'Ivy League',
	'Peaked',
	'Caesar',
	'The Fade',
	'High & Tight',
	'Undercut',
	'Asymmetrical',
	'Flair',
	'Faux Hawk',
	'Top Knot',
	'Comb over Fade'
];
const womenOptions = [
	'Advanced',
	'Classics',
	'Perfect Pixie',
	'Classic Bob',
	'Side Swept Bangs',
	'Layered Crop',
	'French Bob',
	'Blunt Lob',
	'Curls Gone Wild',
	'Layered'
];
const textureOptions = ['Uneven', 'Razored', 'Layered', 'Thinned'];

export default class UpdateSkills extends Component {
	constructor(props) {
		super(props);
		this.state = {
			men: [],
			women: [],
			texture: [],
			xp: 0,
			levelId: 0,
			currentUserId: null,
			currentUser: null,
			currentUserRole: null,
			freelancerId: ''
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
		// this.freelancerId = this.props.location.state.freelancerId;
		// this.setState({ freelancerId: this.freelancerId });
	}

	componentWillMount() {
		let pathLink = this.props.match.path;
		// let response = this.auth.getUser();
		// console.log('response', response)

		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});

		// this.user = this.auth.checkUser();

		// console.log(this.auth.checkUser());

		// console.log('link', link);
		// if (pathLink === '/salon/details') {
		// 	this.render = this.salonForm;
		// } else if (pathLink === '/freelancer/details') {
		// 	this.render = this.freelancerForm;
		// }
	}

	handleChange = e => {
		let { target } = e;
		// const value = target.type === 'checkbox' ? target.checked : target.value;
		const value = Number(target.value);
		const { name } = target;

		this.setState({ [name]: value }, () => {
			if (this.state.xp >= 5) {
				this.setState({ levelId: 2 }, () => {
					console.log(this.state);
				});
			} else if (this.state.xp < 5) {
				this.setState({ levelId: 1 }, () => {
					this.setState({ educatorDayPay: null, educatorEvePay: null });
					console.log(this.state);
				});
			}
		});
	};

	submitForm(e) {
		e.preventDefault();
		console.log(this.state);
		const { currentUserId } = this.state;

		Axios.put(
			'http://localhost:8000/api/freelancers/' + currentUserId,
			this.state
		)
			.then(res => {
				let freelancerId = res.data.freelancerId;
				if (res.status === 200 && this.state.currentUserRole === 2) {
					this.props.history.push({
						pathname: '/dashboard',
						state: {
							freelancerId: freelancerId,
							userRoleId: this.state.currentUserRole
						}
					});
				}
				console.log(res);
			})
			.catch(err => {
				console.error(err);
			});

		// this.props.history.push('/dashboard');
	}

	onChangeMen = event => {
		let menArr = event;
		this.setState({ men: menArr });
	};
	onChangeWomen = event => {
		let womenArr = event;
		this.setState({ women: womenArr });
	};
	onChangeTexture = event => {
		let textureArr = event;
		this.setState({ texture: textureArr });
	};

	render() {
		return (
			<Container className='details-container'>
				<Form noValidate className='form' onSubmit={e => this.submitForm(e)}>
					<Row>
						<Col md={{ size: 6 }}>
							<section>
								<h2>Professional Experience</h2>

								<MyFormGroup
									placeholder='3'
									name='xp'
									formgrouptype='normal'
									label='Experience in the field'
									value={this.state.xp}
									changemethod={this.handleChange}
								/>
							</section>
							<section>
								<h2>Wage per Session</h2>

								<h4>As a Stylist</h4>
								<div>
									<MyFormGroup
										placeholder='180'
										name='stylistDayPay'
										formgrouptype='caption'
										label='Fee for day session (Day-Pay)'
										value={this.state.stylistDayPay}
										changemethod={this.handleChange}
										caption='From 9.30AM to 3.30PM'
										addon='$'
									/>
									<MyFormGroup
										placeholder='250'
										name='stylistEvePay'
										formgrouptype='caption'
										label='Fee for evening session (Eve-Pay)'
										value={this.state.stylistEvePay}
										changemethod={this.handleChange}
										caption='From 4.30PM to 10.30PM'
										addon='$'
									/>
								</div>
							</section>
							<section hidden={this.state.xp >= 5 ? false : true}>
								<h4>As an Educator</h4>
								<div>
									<MyFormGroup
										placeholder='300'
										name='educatorDayPay'
										formgrouptype='caption'
										label='Fee for day session (Day-Pay)'
										value={this.state.educatorDayPay}
										changemethod={this.handleChange}
										caption='From 9.30AM to 3.30PM'
										addon='$'
									/>
									<MyFormGroup
										placeholder='500'
										name='educatorEvePay'
										formgrouptype='caption'
										label='Fee for evening session (Eve-Pay)'
										value={this.state.educatorEvePay}
										changemethod={this.handleChange}
										caption='From 4.30PM to 10.30PM'
										addon='$'
									/>
								</div>
							</section>
						</Col>
						<Col md={{ size: 6 }}>
							<section hidden>
								<h2>Skillset</h2>
								<h4>Pick your prominent skills:</h4>
								<div>
									<h4>Men</h4>
									<div style={{ borderBottom: '1px solid #E9E9E9' }}>
										<CheckboxGroup
											options={menOptions}
											defaultValue={[]}
											onChange={this.onChangeMen}
										/>
									</div>
								</div>
								<div>
									<h4>Women</h4>
									<div style={{ borderBottom: '1px solid #E9E9E9' }}>
										<CheckboxGroup
											options={womenOptions}
											defaultValue={[]}
											onChange={this.onChangeWomen}
										/>
									</div>
								</div>
								<div>
									<h4>Texturizing</h4>
									<div style={{ borderBottom: '1px solid #E9E9E9' }}>
										<CheckboxGroup
											options={textureOptions}
											defaultValue={[]}
											onChange={this.onChangeTexture}
										/>
									</div>
								</div>
							</section>
							<div className='float-right'>
								<Button type='primary' htmlType='submit'>
									Continue
								</Button>
							</div>
						</Col>
					</Row>
				</Form>
			</Container>
		);
	}
}
