import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Steps, Button } from 'antd';
import Axios from 'axios';
import Auth from '../../services/Auth';
import MyCard from '../elements/Card';
import Loader from '../elements/Loader';

export default class ConfirmBookingPage extends Component {
	constructor(props) {
		super(props);
		this.state = { isLoading: true };
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
	}

	async componentWillMount() {
		let freelancerId = Number(this.props.match.params.id);

		this.setState({ freelancerId });

		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState(
					{
						currentUserId: res.data.userId,
						currentUser: res.data.username,
						currentUserRole: res.data.roleId
					},
					() => {
						Axios.post('http://localhost:8000/api/bookings/getTodayBooking', {
							currentUserId: this.state.currentUserId,
							freelancerId: this.state.freelancerId
						})
							.then(res => {
								// console.log(res.data.bookings);
								let bookings = res.data.bookings;
								this.setState({ bookingList: bookings }, () => {
									this.setState({ isLoading: false });
									console.log(this.state);
								});
							})
							.catch(err => {
								console.error(err);
							});
					}
				);
			})
			.catch(err => {
				console.error(err);
			});
	}

	goToDashboard = () => {
		this.props.history.push('/dashboard');
	};

	render() {
		if (this.state.isLoading) {
			return <Loader />;
		}
		return (
			<Container className='booking-container'>
				<Row>
					<div style={{ padding: '0px 10rem', width: '100%' }}>
						<Steps progressDot current={1} size='small'>
							<Steps.Step title='Pick a slot' />
							<Steps.Step title='Confirm booking' />
						</Steps>
					</div>

					<div className='booking-list-container' style={{ width: '100%' }}>
						<h4 className='text-center'>Your bookings for the day</h4>

						<hr />

						<div style={{ width: '100%' }}>
							{this.state.bookingList.map((booking, i) => {
								return (
									<MyCard
										key={booking.bookingId}
										bookingId={booking.bookingId}
										bookingDate={booking.bookingDate}
										status={booking.status}
										slotId={booking.slotId}
										levelId={booking.levelId}
									/>
								);
							})}
						</div>
					</div>

					<div style={{ width: '100%' }}>
						<Button
							type='primary'
							className='float-right'
							onClick={this.goToDashboard}
						>
							Get back to your dashboard
						</Button>
					</div>
				</Row>
			</Container>
		);
	}
}
