import React, { Component } from 'react';
import { Icon, Upload, Modal } from 'antd';
import { Col, Row, Container } from 'reactstrap';
import Auth from '../../services/Auth';
import Axios from 'axios';

function get(params) {}

export default class UploadImage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			previewVisible: false,
			previewImage: '',
			fileList: [
				// {
				//   uid: '-1',
				//   name: 'xxx.png',
				//   status: 'done',
				//   url:
				//     'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
				// }
			]
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
	}
	handleCancel = () => this.setState({ previewVisible: false });

	componentWillMount() {
		let pathLink = this.props.match.path;
		// let response = this.auth.getUser();
		// console.log('response', response)

		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});

		// this.user = this.auth.checkUser();

		// console.log(this.auth.checkUser());

		// console.log('link', link);
		// if (pathLink === '/salon/details') {
		// 	this.render = this.salonForm;
		// } else if (pathLink === '/freelancer/details') {
		// 	this.render = this.freelancerForm;
		// }
	}

	handlePreview = file => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};
	handleChange = ({ fileList }) => this.setState({ fileList });

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const uploadButton = (
			<div>
				<Icon type='plus' />
				<div className='upload-button ant-upload-text'>Upload</div>
			</div>
		);
		return (
			<Container>
				<Row>
					<Col>
						<div className='upload-container clearfix'>
							<Upload
								className='upload-component'
								action='//jsonplaceholder.typicode.com/posts/'
								listType='picture-card'
								fileList={fileList}
								onPreview={this.handlePreview}
								onChange={this.handleChange}
							>
								{fileList.length >= 1 ? null : uploadButton}
							</Upload>
							<Modal
								visible={previewVisible}
								footer={null}
								onCancel={this.handleCancel}
							>
								<img
									alt='example'
									style={{ width: '100%' }}
									src={previewImage}
								/>
							</Modal>
						</div>
					</Col>
				</Row>
			</Container>
		);
	}
}
