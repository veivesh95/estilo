import React, { Component } from 'react';
import { Container, Col, Row } from 'reactstrap';
import ProfileSummary from '../elements/ProfileSummary';
import moment from 'moment';
import { Calendar, Button, notification } from 'antd';
import Auth from '../../services/Auth';
import Axios from 'axios';
import MyCard from '../elements/Card';
import Loader from '../elements/Loader';

class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			freelancerId: null,
			isLoading: true
		};
		this.auth = new Auth();
		this.userToken = this.auth.getToken();
	}

	async componentWillMount() {
		Axios.get('http://localhost:8000/api/auth/whoami', {
			headers: { 'x-access-token': this.userToken }
		})
			.then(res => {
				this.setState({
					currentUserId: res.data.userId,
					currentUser: res.data.username,
					currentUserRole: res.data.roleId
				});
				console.log(this.state);
			})
			.catch(err => {
				console.error(err);
			});

		let freelancerId = this.props.match.params.id;
		this.setState({ freelancerId, isLoading: false }, () => {
			console.log('freelancerId', this.state.freelancerId);

			Axios.get(
				'http://localhost:8000/api/freelancerUserInfo2/' +
					this.state.freelancerId
			).then(res => {
				let freelancerInfo = res.data;
				let name = freelancerInfo.firstName + ' ' + freelancerInfo.lastName;
				this.setState(
					{
						name: name,
						level: freelancerInfo.levelId,
						rating: freelancerInfo.rating,
						experience: freelancerInfo.experience,
						streetNumber: freelancerInfo.streetNumber,
						street: freelancerInfo.street,
						stateCode: freelancerInfo.stateCode,
						contact: freelancerInfo.contact,
						contactWork: freelancerInfo.contactWork,
						email: freelancerInfo.email
					},
					() => {
						Axios.get(
							'http://localhost:8000/api/bookings/countFreelancerBookings2/' +
								this.state.freelancerId
						).then(res => {
							let noOfBookings = res.data.count;
							console.log('noOfBookings', noOfBookings);
							this.setState({ bookingCount: noOfBookings, isLoading: false });
						});
					}
				);
				// this.setState({ isLoading: false });
			});
		});
	}

	goToBooking() {
		// e.preventDefault();
		// console.log(e);
		console.log(this.state);
		let e = this.state.freelancerId;

		if (this.state.currentUserRole === 1) {
			const to = { pathname: `/book/${e}` };
			this.props.history.push(to);
		} else if (this.state.currentUserRole === 2) {
			notification.error({
				message: 'You have to be a Salon Owner to book a freelancer'
			});
		} else {
			this.props.history.push({
				pathname: '/login'
			});
		}
	}

	render() {
		if (this.state.isLoading) {
			return <Loader />;
		}
		return (
			<Container className='profile-container'>
				<Row>
					<Col md={{ size: 6 }}>
						<ProfileSummary
							name={this.state.name}
							level={this.state.level}
							rating={this.state.rating}
							count={this.state.bookingCount}
						/>
						<MyCard
							cardtype='info'
							experience={this.state.experience}
							streetNumber={this.state.streetNumber}
							street={this.state.street}
							stateCode={this.state.stateCode}
							contact={this.state.contact}
							contactWork={this.state.contactWork}
							email={this.state.email}
						/>
					</Col>
					<Col>
						<div className='schedule'>
							<Col>
								<h2 className='section-title'>My Schedule</h2>
							</Col>

							<Col>
								<Button
									type='dashed'
									color='green'
									onClick={() => {
										this.goToBooking();
									}}
								>
									BOOK ME
								</Button>
							</Col>
							<hr />

							<p>Upcoming Bookings</p>
						</div>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default Profile;
