import React, { Component, Fragment } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Welcome from './Welcome';
import Login from './Login';
import Signup from './Signup';
import NotFound from './NotFound';
import Details from './Details';
import UploadImage from './UploadImage';
import UpdateSkills from './UpdateSkills';
import Dashboard from './Dashboard';
import Search from './Search';
import ChooseProfile from './ChooseProfile';
import Profile from './Profile';
import withAuth from '../withAuth';
import { withRouter } from 'react-router-dom';
import SalonDetails from './SalonDetails';
import BookingPage from './BookingPage';
import ConfirmBookingPage from './ConfirmBookingPage';
import MyBookings from './MyBookings';
import Auth from '../../services/Auth';
import BasicHeader from '../elements/BasicHeader';
// import Register from './Register';4

const checkAuth = () => {
	return new Auth().isLoggedIn();
};

const AuthRoute = ({ component: Component, ...rest }) => {
	return (
		<Route
			{...rest}
			render={props =>
				checkAuth() ? (
					<Component {...props} />
				) : (
					<Redirect to={{ pathname: '/login' }} />
				)
			}
		/>
	);
};

const MainRouter = () => {
	// const basicNav = checkAuth() ? null : <BasicHeader />;
	return (
		<Fragment>
			{/* {basicNav} */}
			<main>
				<Switch>
					<Route exact path='/' component={Welcome} />
					<Route exact path='/login' component={Login} />

					{/* <Route exact path='/register' component={Register} /> */}

					<AuthRoute
						exact
						path='/signup/update-skills'
						component={UpdateSkills}
					/>
					<AuthRoute
						exact
						path='/signup/salon-details'
						component={SalonDetails}
					/>
					<AuthRoute
						exact
						path='/signup/image-upload'
						component={UploadImage}
					/>
					<AuthRoute exact path='/signup/profile' component={ChooseProfile} />
					<AuthRoute exact path='/signup/user-details' component={Details} />
					<AuthRoute exact path='/user-details' component={Details} />

					<Route exact path='/signup' component={Signup} />
					{/* 
				{['/salon/details', '/freelancer/details'].map((path, index) => (
					<Route exact path={path} component={Details} key={index} />
				))} */}
					<AuthRoute exact path='/dashboard' component={Dashboard} />

					<Route exact path='/profile/:id' component={Profile} />
					<Route exact path='/profile' component={Profile} />

					<Route exact path='/search' component={Search} />
					<AuthRoute
						exact
						path='/book/confirm-booking/:id'
						component={ConfirmBookingPage}
					/>

					<AuthRoute exact path='/book/:id' component={BookingPage} />

					<AuthRoute exact path='/my-bookings' component={MyBookings} />

					<Route component={NotFound} />
				</Switch>
			</main>
		</Fragment>
	);
};

export default withRouter(MainRouter);
