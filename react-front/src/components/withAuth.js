import React, { Component } from 'react';
import Auth from '../services/Auth';
import { withRouter } from 'react-router-dom';

export default function withAuth(AuthComponent) {
    const auth = new Auth('http://localhost:8000');
    
	return class AuthWrapped extends Component {
		constructor(props) {
			super(props);
			this.state = {
				user: null
			};
		}

		componentWillMount() {
			if (!auth.isLoggedIn()) {
				this.props.history.replace('/login');
			} else {
				try {
					const userProfile = auth.checkUser();
					this.setState({ user: userProfile });
				} catch (err) {
					auth.logout();
					this.props.history.replace('/login');
				}
			}
		}

		render() {
			if (this.state.user) {
				return (
					<AuthComponent history={this.props.history} user={this.state.user} />
				);
			} else {
				return null;
			}
		}
	};
}
