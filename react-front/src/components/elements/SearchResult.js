import React from 'react';
import { Row } from 'reactstrap';
import { Icon } from 'antd';

const SearchResult = props => {
	let element = (
		<Row>
			<div className='result-box' onClick={props.onclick}>
				<div className='result-avatar'>
					<img src='http://avatars.design/wp-content/uploads/2016/09/avatar1a.jpg' />
				</div>
				<div className='result-text'>
					<h4>
						{props.firstName} {props.lastName}
					</h4>
					{/* <p className='muted'>Role</p> */}
					{/* <span>
						{props.streetNumber}, {props.city}
					</span> */}
				</div>
				{/* <div className='result-price float-right'>
					<Icon type='scissor' style={{ fontSize: '12px' }} />
					<span className='counter'>{props.count}</span>
				</div> */}
			</div>
		</Row>
	);

	return <div>{element}</div>;
};

export default SearchResult;
