import React from 'react';

const Input = (props) => {
	let inputElement = null;

	switch (props.inputtype) {
		case 'input':
			inputElement = <input className="myInputElement" {...props} />;
			break;
		case 'textarea':
			inputElement = <textarea className="myInputElement" {...props} />;
			break;
		default:
			inputElement = <input className="myInputElement" {...props} />;
			break;
	}
	return (
		<div className="myInput">
			<label className="myLabel">{props.label}</label>
			{inputElement}
		</div>
	);
};

export default Input;
