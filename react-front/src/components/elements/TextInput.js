import React, { Component } from 'react';
import { withFormik } from 'formik';
import Yup from 'yup';

const TextInput = ({ values }) => {
	return (
		<div>
			<input type='email' name='email' placeholder='Email address' />
		</div>
	);
};

const FormikTextInput = withFormik({
	mapPropsToValues() {
		email: 'test Text';
	}
})(TextInput);

export default TextInput;
