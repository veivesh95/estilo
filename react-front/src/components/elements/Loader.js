import React from 'react';
import ReactLoading from 'react-loading';

const Loader = props => {
	let loaderElement = null;

	switch (props.loadertype) {
		case 'custom':
			loaderElement = (
				<ReactLoading
					type={props.type}
					color={props.color}
					height={props.height}
					width={props.width}
				/>
			);
			break;

		default:
			loaderElement = <ReactLoading type='bubbles' color='#26254B' />;
			break;
	}

	return <div className='loader'>{loaderElement}</div>;
};

export default Loader;
