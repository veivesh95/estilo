import React, { Component } from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	Nav,
	NavItem,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Button
	// NavLink
} from 'reactstrap';
// import { withRouter, router } from 'react-router-dom';
import { Icon, notification, message } from 'antd';
import { NavLink } from 'react-router-dom';
import Auth from '../../services/Auth';
import Axios from 'axios';
import Loader from './Loader';

class Header extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.login = this.login.bind(this);
		this.auth = new Auth();
		this.state = {
			isOpen: false,
			isHidden: false,
			userName: 'User'
		};

		if (this.auth.isLoggedIn()) {
			this.setState({ isHidden: true });
		}
	}

	async componentWillMount() {
		// Axios.get('http://localhost:8000/api/users2/' + ).then(res => {
		// 	let data = res.data;
		// 	let userName = data.firstName + ' ' + data.lastName;
		// 	this.setState({ user: userName });
		// });
	}

	getUser = () => {
		let val = this.auth.checkUser();
		console.log(val);
	};
	login() {
		let checker = this.state.isHidden;
		this.setState({ isHidden: !checker });
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	handleLogout = () => {
		localStorage.removeItem('_token');
		console.log(this.auth.getToken());
		this.props.history.replace('/login');
	};

	goToDashboard = () => {
		if (this.auth.isLoggedIn()) {
			this.props.history.push('/dashboard');
		} else {
			notification.error({
				message: 'Please login to estilo'
			});
		}
	};

	goToMyBookings = () => {
		if (this.auth.isLoggedIn()) {
			this.props.history.push('/my-bookings');
		} else {
			notification.error({
				message: 'Please login to estilo'
			});
		}
	};

	render() {
		if (this.state.isLoading) {
			return <Loader />;
		}
		return (
			<React.Fragment>
				<Navbar
					// fixed={`top`}
					color='light'
					light
					expand='md'
					hidden={this.state.isHidden}
				>
					<NavLink className='navbar-brand' to='/'>
						estilo.
					</NavLink>
					<NavbarToggler onClick={this.toggle} />
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className='ml-auto' navbar>
							<NavItem>
								<NavLink className='nav-link' to='/search'>
									<Icon
										type='search'
										style={{ position: 'relative', top: '-3px', left: '-3px' }}
									/>
									Search
								</NavLink>
							</NavItem>

							<NavItem hidden={!this.state.isHidden}>
								<NavLink className='nav-link' to='/login'>
									Login
								</NavLink>
							</NavItem>
							<NavItem hidden={!this.state.isHidden}>
								<NavLink className='nav-link' to='/signup'>
									Join us
								</NavLink>
							</NavItem>
							<UncontrolledDropdown nav inNavbar hidden={this.state.isHidden}>
								<DropdownToggle nav caret>
									{this.state.userName}
								</DropdownToggle>
								<DropdownMenu right>
									<DropdownItem onClick={this.goToDashboard}>
										Dashboard
									</DropdownItem>
									<DropdownItem onClick={this.goToMyBookings}>
										My Bookings
									</DropdownItem>
									<DropdownItem divider />
									<DropdownItem onClick={this.handleLogout}>
										Logout
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</Nav>
					</Collapse>
				</Navbar>
			</React.Fragment>
		);
	}
}

export default Header;
