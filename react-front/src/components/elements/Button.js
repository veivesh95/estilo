import React from 'react';
import { Button } from 'antd';

const MyButton = props => {
	let buttonElement = null;

	switch (props.buttontype) {
		case 'normal':
			buttonElement = (
				<Button className='myButtonElementDefault' type='default' {...props}>
					{props.name}
				</Button>
			);
			break;
		case 'dashboard-button':
			buttonElement = (
				<Button
					hidden={props.enable}
					className='dashboard dashboard-button'
					type='default'
					size='large'
					onClick={props.onclick}
				>
					{props.name}
				</Button>
			);
			break;
		default:
			buttonElement = <Button className='myButtonElement' {...props} />;
			break;
	}
	return <React.Fragment>{buttonElement}</React.Fragment>;
};

export default MyButton;
