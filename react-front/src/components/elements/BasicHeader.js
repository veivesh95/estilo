import React, { Component } from 'react';
import {
	Nav,
	NavItem,
	NavLink,
	Collapse,
	Navbar,
	NavbarToggler
} from 'reactstrap';

const BasicHeader = () => {
	return (
		<Navbar dark expand='md' className='welcome-nav'>
			{/* <NavbarBrand> */}
			<NavItem
				className='navbar-brand'
				// style={{
				// 	color: 'white',
				// 	fontWeight: 'bold',
				// 	fontSize: '32px',
				// 	cursor: 'arrow'
				// }}
			>
				estilo.
			</NavItem>
			{/* </NavbarBrand> */}
			<NavbarToggler />
			<Collapse isOpen={true} navbar>
				<Nav className='ml-auto' navbar>
					<NavItem style={{ color: 'white' }}>
						<NavLink
							// style={{ color: 'white', fontWeight: 'bold' }}
							href='/login'
						>
							Login
						</NavLink>
					</NavItem>
					<NavItem
						style={{
							// color: 'white',
							border: '1px solid #000',
							borderRadius: '5px'
						}}
					>
						<NavLink
							// style={{ color: 'white', fontWeight: 'bold' }}
							href='/signup'
						>
							Join us
						</NavLink>
					</NavItem>
				</Nav>
			</Collapse>
		</Navbar>
	);
};

export default BasicHeader;
