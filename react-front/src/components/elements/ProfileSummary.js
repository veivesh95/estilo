import React from 'react';
import { Icon, Rate } from 'antd';
import { Col, Row } from 'reactstrap';

const ProfileSummary = props => {
	let level = '';
	//
	if (props.levelId === 1) {
		level = 'Stylist';
	} else if (props.levelId === 2) {
		level = 'Educator';
	}

	let element = (
		<div className='ps-wrapper'>
			<div className='ps-image'>
				<img src='http://avatars.design/wp-content/uploads/2016/09/avatar1a.jpg' />
			</div>
			<div className='ps-content'>
				<div className='ps-text'>
					<i className='freelancer-type'>{level}</i>
					<h3>
						<strong>{props.name}</strong>
					</h3>

					<hr />

					<Row className='freelancer-stats'>
						<Col className='completed'>
							<Icon type='scissor' style={{ fontSize: '16px' }} />
							<span>{props.count}</span>
							<p className='helper-text'>Sessions completed</p>
						</Col>
						{/* <Col className='favs'>
							<Icon type='heart' theme='filled' style={{ fontSize: '16px' }} />
							<span>12</span>
							<p className='helper-text'>Favourited</p>
						</Col> */}
					</Row>
				</div>
			</div>
			<div className='ps-rating text-center'>
				<Rate
					style={{ color: '#b5adf8' }}
					disabled
					defaultValue={props.rating}
				/>
			</div>
		</div>
	);

	return <div>{element}</div>;
};

export default ProfileSummary;
