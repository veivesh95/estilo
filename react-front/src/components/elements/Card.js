import { Card, Button } from 'antd';
import React from 'react';
import Capitalize from '../../helper/Capitalize';
import { Table } from 'reactstrap';

const MyCard = props => {
	let element;
	let slot = '';
	let level = '';
	let status = '';
	let paymentStatus = '';

	//
	if (props.slotId === 1) {
		slot = 'Day';
	} else if (props.slotId === 2) {
		slot = 'Evening';
	}

	//
	if (props.levelId === 1) {
		level = 'Stylist';
	} else if (props.levelId === 2) {
		level = 'Educator';
	}

	//
	if (props.status === 'pending') {
		status = 'Pending';
	} else if (props.status === 'cancelled') {
		status = 'Cancelled';
	} else if (props.status === 'accepted') {
		status = 'Accepted';
	} else if (props.status === 'completed') {
		status = 'Completed';
	}

	//
	if (props.paymentStatus === 'pending') {
		paymentStatus = 'Pending';
	} else if (props.paymentStatus === 'cancelled') {
		paymentStatus = 'Cancelled';
	} else if (props.paymentStatus === 'transferred') {
		paymentStatus = 'Transferred';
	}

	switch (props.cardtype) {
		case 'normal':
			element = (
				<Card
					className='myCard'
					title={`Booking ID: ${props.bookingId}`}
					style={{ width: '100%' }}
				>
					<Table>
						<tbody>
							<tr>
								<th scope='row'>Booking Date</th>
								<td>{props.bookingDate}</td>
							</tr>
							<tr>
								<th scope='row'>Booking Slot</th>
								<td>{slot}</td>
							</tr>
							<tr>
								<th scope='row'>Booked Service</th>
								<td>{level}</td>
							</tr>
							<tr>
								<th scope='row'>Current Booking Status</th>
								<td>{status}</td>
							</tr>
						</tbody>
					</Table>
					<div className='float-right'>
						<Button block type='dashed'>
							Cancel
						</Button>
					</div>
				</Card>
			);
			break;
		case 'bookings':
			element = (
				<Card
					className='myCard'
					title={`Booking ID: ${props.bookingId}`}
					style={{ width: '100%' }}
				>
					<Table borderless>
						<tbody>
							<tr>
								<th scope='row'>Booking Date</th>
								<td>{props.bookingDate}</td>
							</tr>
							<tr>
								<th scope='row'>Booking Slot</th>
								<td>{slot}</td>
							</tr>
							<tr>
								<th scope='row'>Booked Date</th>
								<td>{props.createdDate}</td>
							</tr>
							<tr>
								<th scope='row'>Freelancer</th>
								<td>
									{props.firstName} {props.lastName}
								</td>
							</tr>
							<tr>
								<th scope='row'>Booked Service</th>
								<td>{level}</td>
							</tr>
							<tr>
								<th scope='row'>Amount</th>
								<td>${props.amount}</td>
							</tr>
							<tr>
								<th scope='row'>Current Booking Status</th>
								<td>{status}</td>
							</tr>
						</tbody>
					</Table>
					<div className='float-right'>
						<Button
							type='dashed'
							onClick={props.cancel}
							style={{ marginRight: '12px' }}
						>
							Cancel
						</Button>
						<Button onClick={props.change(props.bookingId)}>Completed</Button>

						{/* {props.change(props.bookingId)} */}
					</div>
				</Card>
			);
			break;
		case 'info':
			element = (
				<Card className='myCard' style={{ width: '100%' }}>
					<Table borderless>
						<tbody>
							<tr>
								<th scope='row'>Experience</th>
								<td>{props.experience}</td>
							</tr>
							<tr>
								<th scope='row'>Street Number</th>
								<td>{props.streetNumber}</td>
							</tr>
							<tr>
								<th scope='row'>Street</th>
								<td>{props.street}</td>
							</tr>
							<tr>
								<th scope='row'>State Code</th>
								<td>{props.stateCode}</td>
							</tr>
							<tr>
								<th scope='row'>Contact</th>
								<td>{props.contact}</td>
							</tr>
							<tr>
								<th scope='row'>Contact (Work)</th>
								<td>{props.contactWork}</td>
							</tr>
							<tr>
								<th scope='row'>Email</th>
								<td>{props.email}</td>
							</tr>
						</tbody>
					</Table>
				</Card>
			);
			break;

		default:
			element = (
				<Card
					className='myCard'
					title={`Booking ID: ${props.bookingId}`}
					style={{ width: '100%' }}
				>
					<Table>
						<tbody>
							<tr>
								<th scope='row'>Booking Date</th>
								<td>{props.bookingDate}</td>
							</tr>
							<tr>
								<th scope='row'>Booking Slot</th>
								<td>{slot}</td>
							</tr>
							<tr>
								<th scope='row'>Booked Service</th>
								<td>{level}</td>
							</tr>
							<tr>
								<th scope='row'>Current Booking Status</th>
								<td>{status}</td>
							</tr>
						</tbody>
					</Table>
					<div className='float-right'>
						<Button block type='dashed'>
							Cancel
						</Button>
					</div>
				</Card>
			);
			break;
	}

	return <div>{element}</div>;
};
export default MyCard;
