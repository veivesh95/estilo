import React from 'react';
import { Checkbox } from 'antd';

const CheckboxGroup = Checkbox.Group;
const MyCheckbox = props => {
  let checkboxElement = null;

  switch (props.checkboxtype) {
    case 'normal':
      checkboxElement = <Checkbox onChange={props.onchange}>Checkbox</Checkbox>;
      break;

    case 'grouped':
      checkboxElement = (
        <div>
          <div style={{ borderBottom: '1px solid #E9E9E9' }}>
            <Checkbox
              indeterminate={props.indeterminate}
              onChange={props.oncheckall}
              checked={props.checkall}
            >
              Check all
            </Checkbox>
          </div>
          <br />
          <CheckboxGroup
            options={props.options}
            value={props.value}
            onChange={props.onchange}
          />
        </div>
      );
      break;

    default:
      checkboxElement = <Checkbox onChange={props.onchange}>Checkbox</Checkbox>;
      break;
  }
  return <React.Fragment>{checkboxElement}</React.Fragment>;
};

export default MyCheckbox;
