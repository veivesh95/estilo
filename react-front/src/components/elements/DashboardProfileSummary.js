import React from 'react';
import { Icon, Col, Row } from 'antd';

const DashboardProfileSummary = props => {
	let style = {
		fontSize: '40px'
	};
	let iconStyle = {
		fontSize: '20px',
		position: 'relative',
		color: '#8071FF',
		top: '-2px',
		left: '-5px'
	};

	let element = null;

	switch (props.profiletype) {
		case 'freelancer':
			element = (
				<div className='summary-cont'>
					<div className='image-cont'>
						<img
							src='http://avatars.design/wp-content/uploads/2016/09/avatar1a.jpg'
							className='image'
						/>
					</div>

					<Row>
						<div className='icon-cont'>
							<Col>
								<Icon style={iconStyle} type='scissor' />
								<span className='icon-text' style={style}>
									<span style={{ fontSize: '14px' }}>{props.sessions}</span>
								</span>
							</Col>
							{/* <Col span={8}>
								<Icon style={iconStyle} theme='filled' type='star' />
								<span className='icon-text' style={style}>
									<span style={{ fontSize: '14px' }}>{props.rating}</span>
								</span>
							</Col> */}
							{/* <Col span={8}>
								<Icon style={iconStyle} type='heart' theme='filled' />
								<span className='icon-text' style={style}>
									{props.favs}
								</span>
							</Col> */}
						</div>
					</Row>

					<Row>
						<Col className='text-center'>
							<h3>{props.freelancerName}</h3>
							<span className='muted'>
								<i>{props.freelancerClass}</i>
							</span>
						</Col>
					</Row>
				</div>
			);
			break;

		case 'salon':
			element = (
				<div className='summary-cont'>
					<div className='image-cont'>
						<img src='https://imgur.com/6Ioub1q.png' className='image' />
					</div>

					<Row>
						<div className='icon-cont'>
							<Col>
								<Icon style={iconStyle} type='schedule' theme='filled' />
								<span className='icon-text' style={style}>
									<span style={{ fontSize: '14px' }}>
										{props.sessions} Bookings
									</span>
								</span>
							</Col>
							{/* <Col span={12}>
								<Icon style={iconStyle} type='heart' theme='filled' />
								<span className='icon-text' style={style}>
									{props.favs}
								</span>
							</Col> */}
						</div>
					</Row>

					<Row>
						<Col className='text-center'>
							<h3>{props.salonName}</h3>
							{/* <span className='muted'>
								<i>{props.freelancerClass}</i>
							</span> */}
						</Col>
					</Row>
				</div>
			);
			break;
	}

	return <React.Fragment>{element}</React.Fragment>;
};

export default DashboardProfileSummary;
