import React from 'react';
import { Container } from 'reactstrap';

const Footer = () => {
	return (
		<footer
			className='footer'
			style={{ backgroundColor: 'black', textAlign: 'center', color: '#fff' }}
		>
			<Container>
				<span color='muted'>
					Made with <span class='heart'>💙</span> React
				</span>
			</Container>
		</footer>
	);
};

export default Footer;
