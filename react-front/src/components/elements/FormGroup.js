import React from 'react';
import { Label } from 'reactstrap';

import { Input, Form, InputNumber, Tooltip, Icon } from 'antd';
import MySelect from './Select';

const MyFormGroup = props => {
	let formGroupElement = null;

	switch (props.formgrouptype) {
		case 'normal':
			formGroupElement = (
				<Form.Item>
					<Label>{props.label}</Label>
					<Input
						required
						autoFocus={props.focus}
						type={props.type}
						name={props.inputname}
						title={props.title}
						placeholder={props.placeholder}
						value={props.value}
						onChange={props.changemethod}
						pattern={props.pattern}
						{...props}
					/>
				</Form.Item>
			);
			break;

		case 'ein':
			formGroupElement = (
				<Form.Item>
					<Label>
						{props.label}
						<Tooltip title='Your unique Federal Employee Identification Number'>
							<Icon
								style={{ top: '12px', position: 'relative', left: '5px' }}
								type='question-circle-o'
							/>
						</Tooltip>
					</Label>
					<br />
					<InputNumber
						pattern={props.pattern}
						style={{ width: '100%' }}
						required
						size='default'
						// maxLength={9}
						len={props.length}
						// min={props.min}
						// max={props.max}
						name={props.inputname}
						placeholder={props.placeholder}
						value={props.value}
						onChange={props.changemethod}
						{...props}
					/>
				</Form.Item>
			);
			break;

		case 'caption':
			formGroupElement = (
				<Form.Item>
					<Label>{props.label}</Label>
					<span className='muted'> ({props.caption})</span>
					<Input
						required
						type={props.type}
						name={props.inputname}
						placeholder={props.placeholder}
						value={props.value}
						onChange={props.changemethod}
						addonBefore={props.addon}
						{...props}
					/>
				</Form.Item>
			);
			break;

		case 'search-select':
			formGroupElement = (
				<Form.Item>
					<Label>{props.label}</Label>
					<MySelect
						selecttype='normal'
						selectchangemethod={props.selectchangemethod}
						option={props.option}
					/>
				</Form.Item>
			);
			break;

		case 'search-select-group':
		// formGroupElement = (
		//   <Select
		//     defaultValue={props.default}
		//     onChange={props.changemethod}
		//   >
		//     <OptGroup label={props.}>
		//       <Option value='jack'>Jack</Option>
		//       <Option value='lucy'>Lucy</Option>
		//     </OptGroup>
		//     <OptGroup label='Engineer'>
		//       <Option value='Yiminghe'>yiminghe</Option>
		//     </OptGroup>
		//   </Select>
		// );
		default:
			formGroupElement = <input className='' {...props} />;
			break;
	}
	return <React.Fragment>{formGroupElement}</React.Fragment>;
};

export default MyFormGroup;

/*
<FormGroup>
                <Label>Salon Name</Label>
                <Input
                  type='text'
                  name='name'
                  placeholder='Magical Hair'
                  value={this.state.name}
                  onChange={e => {
                    //   this.validateName(e);
                    this.handleChange(e);
                  }}
                />
                <FormFeedback valid>Nice!</FormFeedback>
                <FormFeedback>We need to know your place!</FormFeedback>
                <FormText>Your username is most likely your email.</FormText> 
                </FormGroup>

*/
