import React from 'react';
import { Select } from 'antd';

const Option = Select.Option;

const MySelect = props => {
  let selectElement = null;
  let optionArr = props.option;

  switch (props.selecttype) {
    case 'normal':
      selectElement = (
        <Select
          defaultValue={props.default}
          onChange={props.selectchangemethod}
        >
          <Option value='lucy'>lucy</Option>
        </Select>
      );
      break;

    // case 'grouped':
    //   selectElement = (
    //     <div>
    //       <div style={{ borderBottom: '1px solid #E9E9E9' }}>
    //         <Checkbox
    //           indeterminate={props.indeterminate}
    //           onChange={props.oncheckall}
    //           checked={props.checkall}
    //         >
    //           Check all
    //         </Checkbox>
    //       </div>
    //       <br />
    //       <CheckboxGroup
    //         options={props.options}
    //         value={props.value}
    //         onChange={props.onchange}
    //       />
    //     </div>
    //   );
    //   break;

    default:
      //   selectElement = <Checkbox onChange={props.onchange}>Checkbox</Checkbox>;
      break;
  }
  return <React.Fragment>{selectElement}</React.Fragment>;
};

export default MySelect;
