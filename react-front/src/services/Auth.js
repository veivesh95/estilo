import React from 'react';
import decode from 'jwt-decode';
import axios from 'axios';

export default class Auth {
	constructor(domain) {
		this.domain = 'http://localhost:8000';

		this.setToken = this.setToken.bind(this);
		this.getToken = this.getToken.bind(this);
		this.logout = this.logout.bind(this);
		this.checkUser = this.checkUser.bind(this);
		this.login = this.login.bind(this);
		this.isLoggedIn = this.isLoggedIn.bind(this);
		this.isTokenExpired = this.isTokenExpired.bind(this);
		this.fetch = this.fetch.bind(this);
		this._checkStatus = this._checkStatus.bind(this);
		this.register = this.register.bind(this);
	}

	register(user) {
		axios.post(`${this.domain}/api/users/signup`, user).then(res => {
			this.setToken(res.token);
			return Promise.resolve(res);
		});
	}

	setToken(token) {
		localStorage.setItem('_token', token);
	}

	getToken() {
		return localStorage.getItem('_token');
	}

	logout() {
		localStorage.removeItem('_token');
	}

	checkUser() {
		return decode(this.getToken());
	}

	getUser() {
		if (this.isLoggedIn) {
			let userToken = this.getToken();
			axios
				.get(this.domain + 'api/auth/whoami', {
					headers: { 'x-access-token': userToken }
				})
				.then(res => {
					console.log(res);
					return Promise.resolve(res);
				})
				.catch(err => {
					console.error(err);
				});
		}
	}

	login(user) {
		axios
			.post(this.domain + '/api/login', user)
			.then(res => {
				this.setToken(res.data.token);
				return Promise.resolve(res);
			})
			.catch(err => {
				return err;
			});
	}

	isLoggedIn() {
		let token = this.getToken();
		return !!token && !this.isTokenExpired(token);
	}

	isTokenExpired(token) {
		try {
			const decoded = decode(token);
			if (decoded.exp < Date.now() / 1000) {
				// Checking if token is expired. N
				return true;
			} else return false;
		} catch (err) {
			return false;
		}
	}

	fetch(url, options) {
		// performs api calls sending the required authentication headers
		const headers = {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		};

		// Setting Authorization header
		// Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
		if (this.isLoggedIn()) {
			headers['Authorization'] = 'Bearer ' + this.getToken();
		}

		return fetch(url, {
			headers,
			...options
		})
			.then(this._checkStatus)
			.then(response => response.json());
	}

	_checkStatus(response) {
		// raises an error in case response status is not a success
		if (response.status >= 200 && response.status < 300) {
			// Success status lies between 200 to 300
			return response;
		} else {
			var error = new Error(response.statusText);
			error.response = response;
			throw error;
		}
	}
}
